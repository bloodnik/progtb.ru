<?php
	$site_open = $this->settings_lib->item('auth.allow_register');
?>

<body class="login">
<!-- BEGIN LOGO -->
<div class="logo">
    <a href="index.html">
        <img src="assets/frontend/onepage/img/logo/blue.png" alt=""/>
    </a>
</div>
<!-- END LOGO -->
<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
<div class="menu-toggler sidebar-toggler">
</div>
<!-- END SIDEBAR TOGGLER BUTTON -->
<!-- BEGIN LOGIN -->
<div class="content">
    <!-- BEGIN LOGIN FORM -->
    <?$attributes = array('class' => 'login-form', 'autocomplete' => 'off');?>
    <?php echo form_open(LOGIN_URL, $attributes); ?>
        <?php echo Template::message(); ?>
        <?php
        if (validation_errors()) :
            ?>
            <div class="row-fluid">
                <div class="span12">
                    <div class="alert alert-danger fade in">
                        <a data-dismiss="alert" class="close">&times;</a>
                        <?php echo validation_errors(); ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>
        <h3 class="form-title">Войти в админ панель</h3>
        <div class="alert alert-danger display-hide">
            <button class="close" data-close="alert"></button>
			<span>
			Введите имя пользователя и пароль. </span>
        </div>
        <div class="form-group <?php echo iif( form_error('login') , 'error') ;?>">
            <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
            <label class="control-label visible-ie8 visible-ie9">Логин</label>
            <div class="input-icon">
                <i class="fa fa-user"></i>
                <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Логин"  name="login" id="login_value" value="<?php echo set_value('login'); ?>" tabindex="1" placeholder="<?php echo $this->settings_lib->item('auth.login_type') == 'both' ? lang('bf_username') .'/'. lang('bf_email') : ucwords($this->settings_lib->item('auth.login_type')) ?>"/>
            </div>
        </div>
        <div class="form-group <?php echo iif( form_error('password') , 'error') ;?>">
            <label class="control-label visible-ie8 visible-ie9">Пароль</label>
            <div class="input-icon">
                <i class="fa fa-lock"></i>
                <input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="Пароль" name="password" id="password" tabindex="2" value="<?php echo lang('bf_password'); ?>"/>
            </div>
        </div>
        <?php if ($this->settings_lib->item('auth.allow_remember')) : ?>
            <div class="form-actions">
                <label class="checkbox">
                    <input type="checkbox" name="remember_me" id="remember_me" value="1" tabindex="3"/> <?php echo lang('us_remember_note'); ?> </label>
                <button type="submit" class="btn blue pull-right" name="log-me-in" id="submit" value="<?php e(lang('us_let_me_in')); ?>">
                    Войти <i class="m-icon-swapright m-icon-white"></i>
                </button>
            </div>
        <?endif;?>
        <div class="form-actions">
            <button id="register-back-btn" type="button" class="btn" onclick="window.location = '<?php echo site_url(); ?>'">
                <i class="m-icon-swapleft"></i> Назад </button>
        </div>
    <?php echo form_close(); ?>
    <?php // show for Email Activation (1) only
    if ($this->settings_lib->item('auth.user_activation_method') == 1) : ?>
        <!-- Activation Block -->
        <p style="text-align: left" class="well">
            <?php echo lang('bf_login_activate_title'); ?><br />
            <?php
            $activate_str = str_replace('[ACCOUNT_ACTIVATE_URL]',anchor('/activate', lang('bf_activate')),lang('bf_login_activate_email'));
            $activate_str = str_replace('[ACTIVATE_RESEND_URL]',anchor('/resend_activation', lang('bf_activate_resend')),$activate_str);
            echo $activate_str; ?>
        </p>
    <?php endif; ?>
    <p style="text-align: center">
        <?php if ( $site_open ) : ?>
            <?php echo anchor(REGISTER_URL, lang('us_sign_up')); ?>
        <?php endif; ?>

        <br/><?php echo anchor('/forgot_password', lang('us_forgot_your_password')); ?>
    </p>
    <!-- END LOGIN FORM -->
</div>
<!-- END LOGIN -->
<!-- BEGIN COPYRIGHT -->
<div class="copyright">
    2014 &copy; ПрогТехБизнес - Вход в админ панель.
</div>
<!-- END COPYRIGHT -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="../../assets/global/plugins/respond.min.js"></script>
<script src="../../assets/global/plugins/excanvas.min.js"></script>
<![endif]-->
<script src="../../assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="../../assets/global/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="../../assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/backstretch/jquery.backstretch.min.js" type="text/javascript"></script>
<script type="text/javascript" src="../../assets/global/plugins/select2/select2.min.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="../../assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="../../assets/admin/layout3/scripts/layout.js" type="text/javascript"></script>
<script src="../../assets/admin/layout3/scripts/demo.js" type="text/javascript"></script>
<script src="../../assets/admin/pages/scripts/login-soft.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script>
    jQuery(document).ready(function() {
        Metronic.init(); // init metronic core components
        Layout.init(); // init current layout
        Demo.init(); // init demo features
        Login.init();
        // init background slide images
        $.backstretch([
                "../../assets/admin/pages/media/bg/1.jpg",
                "../../assets/admin/pages/media/bg/2.jpg",
                "../../assets/admin/pages/media/bg/3.jpg",
                "../../assets/admin/pages/media/bg/4.jpg"
            ], {
                fade: 1000,
                duration: 8000
            }
        );
    });
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->