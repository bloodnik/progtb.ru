<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<body class="menu-always-on-top">
<div class="header header-mobi-ext">
    <div class="container">
        <div class="row">
            <div class="col-md-2 col-sm-2">
                <a class="scroll site-logo" href="#promo-block"><img src="assets/frontend/onepage/img/logo/blue.png"
                                                                     alt="Логотип компании Прогрессивные Технологии Бизнеса"></a>
            </div>
            <a href="javascript:void(0);" class="mobi-toggler"><i class="fa fa-bars"></i></a>

            <!-- Navigation BEGIN -->
            <div class="col-md-10 pull-right">
                <ul class="header-navigation">
                    <li class="current"><a href="#promo-block">Начало</a></li>
                    <li><a href="#about">О нас</a></li>
                    <li><a href="#services">Мы предлагаем и делаем</a></li>
                    <li><a href="#team">Наша Команда</a></li>
                    <li><a href="#blog">Блог</a></li>
                    <li><a href="#portfolio">Проекты</a></li>
                    <li><a href="#feedback">Отзывы</a></li>
                    <li><a href="#contact">Контакты</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="promo-block" id="promo-block">
    <div class="tp-banner-container">

            <!-- div class="alert alert-info alert-dismissible animated bounceInDown col-md-3 col-md-offset-7" role="alert"
                 style="position: fixed;  margin-top: 65px;  z-index: 999; text-align: center" data-delay="5000">
                <button type="button" class="close" data-dismiss="alert" aria-label="Закрыть"><span
                        aria-hidden="true">&times;</span></button>
                <strong style="font-size: 18px; margin-bottom: 20px">ЗАПИСЬ НА ТРЕНИНГ КУРС!</strong><br/>
                Разработка на 1С по промышленным стандартам
                <hr/>
                <div id="countdown" style="overflow: hidden; font-weight: bold; font-size: 16px; background: none; border: none;"></div>
                <div class="col-md-12" style="text-align: center; margin-top: 20px"><a href="http://progtb.ru/event" class="btn btn-danger" target="_blank">Узнать подробности</a></div>
            </div -->

        <div class="tp-banner">
            <ul>
                <li data-transition="fade" data-slotamount="5" data-masterspeed="700" data-delay="9400"
                    class="slider-item-1">
                    <img src="assets/frontend/onepage/img/silder/slide1.jpg" alt="" data-bgfit="cover"
                         data-bgposition="center center" data-bgrepeat="no-repeat">

                    <div class="tp-caption large_text customin customout start"
                         data-x="center"
                         data-hoffset="0"
                         data-y="center"
                         data-voffset="60"
                         data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                         data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                         data-speed="1000"
                         data-start="500"
                         data-easing="Back.easeInOut"
                         data-endspeed="300">
                        <div class="promo-like"><i class="fa fa-cogs"></i></div>
                        <div class="promo-like-text">
                            <h2>Инструменты управления</h2>

						<p><a href="#portfolio">Решения для управления бизнесом</a><br><a href="#services">Узнать как мы это делаем</a></p>
                        </div>
                    </div>
                    <div class="tp-caption large_bold_white fade"
                         data-x="center"
                         data-y="center"
                         data-voffset="-110"
                         data-speed="300"
                         data-start="1700"
                         data-easing="Power4.easeOut"
                         data-endspeed="500"
                         data-endeasing="Power1.easeIn"
                         data-captionhidden="off"
                         style="z-index: 6">Автоматизация <span>начинается</span> с нас
                    </div>
                </li>

                <li data-transition="fade" data-slotamount="5" data-masterspeed="700" data-delay="9400"
                    class="slider-item-3">

                    <div class="tp-caption tp-fade fadeout fullscreenvideo"
                         data-x="0"
                         data-y="0"
                         data-speed="1000"
                         data-start="1100"
                         data-easing="Power4.easeOut"
                         data-endspeed="1500"
                         data-endeasing="Power4.easeIn"
                         data-autoplay="true"
                         data-autoplayonlyfirsttime="false"
                         data-nextslideatend="true"
                         data-forceCover="1"
                         data-dottedoverlay="twoxtwo"
                         data-aspectratio="16:9"
                         data-forcerewind="on"
                         style="z-index: 2">

                        <video class="video-js vjs-default-skin" preload="none" width="100%" height="100%">
                            <source src='http://goodwebtheme.com/previewvideo/forest_edit.mp4' type='video/mp4'>
                            <source src='http://goodwebtheme.com/previewvideo/forest_edit.webm' type='video/webm'>
                            <source src='http://goodwebtheme.com/previewvideo/forest_edit.ogv' type='video/ogg'>
                        </video>
                    </div>

                    <div class="tp-caption large_bold_white_25 customin customout tp-resizeme"
                         data-x="center" data-hoffset="0"
                         data-y="170"
                         data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:5;scaleY:5;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                         data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                         data-speed="600"
                         data-start="1400"
                         data-easing="Power4.easeOut"
                         data-endspeed="600"
                         data-endeasing="Power0.easeIn"
                         style="z-index: 3">Приводим из "непроходимого леса"<br/>к полноценной системе управления
                        бизнесом.
                    </div>

                    <div class="tp-caption medium_text_shadow customin customout tp-resizeme"
                         data-x="center" data-hoffset="0"
                         data-y="bottom" data-voffset="-140"
                         data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:5;scaleY:5;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                         data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                         data-speed="600"
                         data-start="1700"
                         data-easing="Power4.easeOut"
                         data-endspeed="600"
                         data-endeasing="Power0.easeIn"
                         style="z-index: 4">Прогрессивные Технологии Бизнеса
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>
<!-- About block BEGIN -->
<div class="about-block content content-center" id="about">
    <div class="container">
        <h2>Компания <strong>Прогрессивные Технологии Бизнеса</strong></h2>
        <h4>Профессионалы в области разработки, автоматизации и внедрения бизнес-решений</h4>

        <p>Компания «Прогрессивные Технологии Бизнеса» на сегодняшний день является одной из наиболее успешных компаний
            в области разработки, автоматизации и внедрения бизнес-решений, широко используемых в целях управления
            организацией. Образованная около 10 лет назад, компания достигла высоких результатов. Наши клиенты ведут
            свой бизнес в различных городах России. География наших внедрений широка: Центральный федеральный округ,
            Южный федеральный округ, Сибирский федеральный округ, Уральский федеральный округ, Приволжский федеральный
            округ.</p>
        <br/>

        <div class="ab-trio">
            <img src="assets/frontend/onepage/img/trio.png" alt="" class="img-responsive">

            <div class="ab-cirlce ab-cirle-blue">
                <i class="fa fa-recycle"></i>
                <strong>Анализ</strong>
            </div>
            <div class="ab-cirlce ab-cirle-red">
                <i class="fa fa-code"></i>
                <strong>Программирование</strong>
            </div>
            <div class="ab-cirlce ab-cirle-green">
                <i class="fa fa-users"></i>
                <strong>Обучение</strong>
            </div>
        </div>
    </div>
</div>
<!-- About block END -->

<!-- Services block BEGIN -->
<div class="services-block content content-center" id="services">
    <div class="container">
        <h2>Наши <strong>услуги</strong></h2>
        <h4>Чем мы занимаемся и что можем предложить</h4>

        <div class="row">
            <? foreach ($services as $service): //Заполняем наши услуги?>
                <div class="col-md-6 col-sm-6 col-xs-12 item">
                    <i class="<?= $service["IMG"] ?>"></i>

                    <h3><?= $service["NAME"] ?></h3>

                    <p><?= $service["DESCRIPTION"] ?></p>
                </div>
            <? endforeach; ?>
        </div>
    </div>
    <!-- Choose us block BEGIN -->
    <div class="choose-us-block content text-center margin-bottom-40" id="benefits">
        <div class="container">
            <h2>Почему <strong>Прогрессивные Технологии Бизнеса</strong></h2>

            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12 text-left">
                    <img src="assets/frontend/onepage/img/choose-us.png" alt="Why to choose us"
                         class="img-responsive">
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12 text-left">
                    <div class="panel-group" id="accordion1">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h5 class="panel-title">
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1"
                                       href="#accordion1_1">Наша миссия и приоритеты</a>
                                </h5>
                            </div>
                            <div id="accordion1_1" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <p>Миссия ПТБ - <strong>«Мы помогаем бизнесу»</strong> - состоит их трех слов,
                                        которые характеризуют приоритеты нашей работы. В каждое из этих слов вложен
                                        тайный смысл, который мы с радостью раскрываем нашим клиентам.
                                        <br/>Слово <strong>«МЫ»</strong> означает, что мы организуем внутреннюю среду
                                        нашей компании таким образом, чтобы выполнять работы на самом высоком уровне.
                                        Мы приглашаем на работу опытных и квалифицированных работников и не используем
                                        схемы субподрядов, чтобы обеспечить наилучшее качество работ и соблюдение
                                        сроков.
                                        <br/>Слово <strong>«ПОМОГАЕМ»</strong> - раскрывает главный наш приоритет,
                                        поскольку, для нас действительно важно, чтобы наша работа реально помогла
                                        клиенту достичь желаемого результата. Нам интересны сложные проекты, решающие
                                        ключевые задачи.
                                        <br/>И, наконец, слово <strong>«БИЗНЕСУ»</strong> характеризует наших
                                        клиентов, в которых мы видим деловых людей, которые уделяют должное внимание
                                        развитию своего дела и делают все для того, чтобы быть конкурентно
                                        привлекательными в своей сфере.</p></div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h5 class="panel-title">
                                    <a class="accordion-toggle collapsed" data-toggle="collapse"
                                       data-parent="#accordion1" href="#accordion1_2">Стиль нашей работы</a>
                                </h5>
                            </div>
                            <div id="accordion1_2" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p>Стиль ПТБ - это что-то среднее между фьюжном и классикой, с элементами рока и
                                        джазовыми импровизациями.<br/>
                                        <br/>А теперь попытаемся внести ясность:
                                        <br/>В своей работе за основу мы берем классические теории (разработки,
                                        управления процессами, работы с клиентами, работы с персоналом), которые
                                        результативны в долгосрочной перспективе и понятны нашим клиентам (в отличие
                                        от стиля «панк»).
                                        <br/>Некоторые неэффективные инструменты классических теорий мы заменяем на
                                        современные технологии с различных сфер. Эдаким «фьюжном» мы достигаем
                                        результат на краткосрочном горизонте в решении конкретных задач в рамках
                                        проекта.
                                        <br/>Рок - это наш вектор на результат, невзирая ни на какие трудности и
                                        противостояния. Если бизнес-процесс нужно поменять, значит, мы его поменяем.
                                        <br/>Зачем тут еще и джаз? Джазовые импровизации позволяют решить нерешаемую
                                        задачу, когда мы находим нестандартный выход из, казалось бы, тупиковой
                                        ситуации. Джаз придает свежесть ума и полет фантазии…</p></div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h5 class="panel-title">
                                    <a class="accordion-toggle collapsed" data-toggle="collapse"
                                       data-parent="#accordion1" href="#accordion1_3">Мы через 10 лет</a>
                                </h5>
                            </div>
                            <div id="accordion1_3" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p>Мы постоянно работаем над своим развитием.
                                        <br/>Рост ПТБ - не дело одного года, поскольку за ростом филиальной сети мы не
                                        хотим потерять качества своей работы и перестать видеть наших клиентов.
                                        <br/>В течение ближайших лет мы планируем расширять и дальше географию своих
                                        внедрений и направлений разработки.
                                        <br/>При назначении управленцев мы отдаем предпочтение своим кадрам, полагая,
                                        что сотрудник, выросший на нашей философии, сохранит ее в работе с клиентом.
                                        <br/>Большое внимание мы уделим недорогим типовым решениям, основанным на
                                        нашем опыте и разработках, которые приблизят к нам клиентов с небольшими
                                        финансовыми возможностями.
                                        <br/>Прогресс и технологии не стоят на месте, и мы продолжим идти, опережая
                                        время.</p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h5 class="panel-title">
                                    <a class="accordion-toggle collapsed" data-toggle="collapse"
                                       data-parent="#accordion1" href="#accordion1_4">Чем мы отличаемся от других</a>
                                </h5>
                            </div>
                            <div id="accordion1_4" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p><strong>Мы профессиональны. </strong>Сотрудники ПТБ - люди с высшим
                                        профессиональным образованием и опытом работы в различных сферах с активной
                                        жизненной позицией, мотивированные на результат.
                                        <br/><strong>Мы современны. </strong>При выполнении работ мы предлагаем
                                        клиентам самые современные программные решения и технологии.
                                        <br/><strong>Мы мобильны. </strong>По просьбе клиента наши сотрудники готовы
                                        выехать для выполнения работ в любую точку мира.
                                        <br/><strong>Мы молоды. </strong>Средний возраст сотрудников компании не
                                        превышает 35 лет.
                                        <br/><strong>Мы организованны. </strong>Грамотная система управления проектами
                                        и отработанные методики разработки и внедрения позволяют нам достигать
                                        результат в минимальные сроки.
                                        <br/><strong>Мы дружны. </strong>Мы устраиваем совместные мероприятия для
                                        поддержания теплой атмосферы и взаимопонимания в коллективе.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Choose us block END -->
</div>
<!-- Services block END -->

<!-- Message block BEGIN -->
<div class="message-block content content-center valign-center" id="message-block">
    <div class="valign-center-elem">
        <h2>Кто владеет информацией<strong>тот владеет миром</strong></h2>
        <em>Ротшильд</em>
    </div>
</div>
<!-- Message block END -->

<!-- Team block BEGIN -->
<div class="team-block content content-center margin-bottom-40" id="team">
    <div class="container">
        <h2>Знакомьтесь <strong>прогрессивные технологии бизнеса</strong></h2>
        <h4>Коллектив ПТБ представляет собой дружную и сплоченную команду профессионалов. Нас можно сравнить со
            струнным оркестром, в котором каждый из нас представляет собой определенный инструмент. Чем лучше каждый
            справляется со своей партией и чем лучше работает дирижер - тем красивее музыка и более доволен
            слушатель.</h4>

        <div class="row">
            <div class="col-md-3 item">
                <img src="http://progtb.ru/assets/images/4.jpg" alt="Инженеры" class="img-responsive">

                <h3>Mr. Alex Aniskov</h3>
                <em>Генереальный директор</em>

                <p>30 лет. Молодой талантливый руководитель, способный настроить коллектив на проект любой сложности и
                    масштаба. Подобно дирижеру, добивается «наилучшего звучания» каждого специалиста в рамках общей
                    работы. Мастерство и опыт позволяют ему в наиболее сложных ситуациях заменить любой из
                    инструментов или дополнить оркестр новым.</p>
            </div>
            <div class="col-md-3 item">
                <img src="http://progtb.ru/assets/images/1.jpg" alt="Аналитики" class="img-responsive">

                <h3>Аналитическая служба</h3>
                <em>Аналитики</em>

                <p>Состоит из людей с профессиональным (экономическим, техническим) образованием и большим опытом
                    работы в различных сферах. Аналитик - главная скрипка оркестра. Основные задачи аналитика: понять,
                    что хочет клиент, предложить клиенту варианты, спроектировать решение, внедрить и обучить
                    персонал. Аналитик говорит с заказчиком на его языке, ретранслируя задачи другим службам ПТБ.</p>
            </div>
            <div class="col-md-3 item">
                <img src="http://progtb.ru/assets/images/2.jpg" alt="Девелоперы" class="img-responsive">

                <h3>Служба разработки</h3>
                <em>Девелоперы</em>

                <p>Программисты - наши виолончели. Это те люди, которые, искусно обращаясь с кодом, создают само
                    программное решение, которое будет решать поставленные задачи. От их работы зависит насколько
                    удобным и корректным будет использование программного продукта.</p>
            </div>
            <div class="col-md-3 item">
                <img src="http://progtb.ru/assets/images/3-2.jpg" alt="Инженеры" class="img-responsive">

                <h3>Техническая служба</h3>
                <em>Инженеры</em>

                <p>Это контрабас, задающий ритм. Специалисты и инженеры технической службы осуществляют установку,
                    настройку, оптимизацию программных продуктов и оборудования. От их работы зависит техническая
                    сторона функционирования автоматизированной информационной среды клиента, скорость и качество
                    работы ее элементов.</p>
            </div>

        </div>
    </div>
</div>
<!-- Team block END -->

<!-- Prices block BEGIN -->
<div class="prices-block content content-center" id="blog">
    <div class="container">
        <h2 class="margin-bottom-50"><strong><a href="/blog">Блог</a></strong></h2>

        <div class="row">
            <? foreach ($blog as $item): ?>
                <!-- Pricing item BEGIN -->
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="pricing-item">
                        <div class="pricing-head">
                            <h3><a href="/blog/detail/<?= $item->code ?>"><?= $item->name ?></a></h3>
                        </div>
                        <div class="pricing-content">
                            <div class="pi-price">

                                <div class="img-cover">
                                    <? if (strlen($item->name) > 0): ?>
                                        <img src="<?= $item->image ?>" alt="<?= $item->name ?>" style="width: 100%"/>
                                    <? endif; ?>
                                </div>
                            </div>
                            <p><?= word_limiter($item->text, 40) ?></p>
                        </div>
                        <div class="pricing-footer">
                            <a class="btn btn-default" href="/blog/detail/<?= $item->code ?>">Подробнее</a>
                        </div>
                    </div>
                </div>
            <? endforeach ?>
            <!-- Pricing item END -->
        </div>
    </div>
</div>
<!-- Prices block END -->

<!-- Portfolio block BEGIN -->
<div class="portfolio-block content content-center" id="portfolio">
    <div class="container">
        <h2 class="margin-bottom-50">Предыдущие <strong>проекты</strong></h2>
    </div>
    <div class="row">
        <? foreach ($projects as $project): //Заполняем наши проекты?>
            <div class="item col-md-2 col-sm-6 col-xs-12">
                <img src="<?= $project["img"] ?>" alt="<?= $project["name"] ?>" class="img-responsive">
                <a href="#pop-up<?= $project["id"] ?>" class="zoom valign-center">
                    <div class="valign-center-elem">
                        <strong><?= $project["name"] ?></strong>
                        <em><?= $project["slogan"] ?></em>
                        <!-- <b>О проекте ПромПит</b> -->
                    </div>
                </a>
                <!-- BEGIN fast view of a product -->
                <div id="pop-up<?= $project["id"] ?>" style="display: none; width: 930px; overflow:none">
                    <div class="product-page product-pop-up content">
                        <div class="row" style="margin:0">
                            <div class="col-md-6 col-sm-6 col-xs-3">
                                <div class="">
                                    <img src="<?= $project["img"] ?>" alt="<?= $project["name"] ?>" class="">
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-9 content-center ">
                                <div class="white-back">
                                    <h2><strong><?= $project["name"] ?></strong></h2>
                                    <h4>Описание проекта</h4>

                                    <div class="description">
                                        <p><?= $project["description"] ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="front-lists-v1">
                            <div class="col-md-4">

                            </div>
                        </div>
                    </div>
                </div>
                <!-- END fast view of a product -->
            </div>
        <? endforeach; ?>
    </div>
</div>
<!-- Portfolio block END -->

<!-- Checkout block BEGIN -->
<div class="checkout-block content">
    <div class="container">
        <div class="row">
            <div class="col-md-10">
                <h2>Мониторинг <em>Личный кабинет для просмотра состояния работ по проектам в реальном времени.</em>
                </h2>
            </div>
            <div class="col-md-2 text-right">
                <a href="http://progtb.ru/demo" target="_blank" class="btn btn-primary">Демонстрация</a>
            </div>
        </div>
    </div>
</div>
<!-- Checkout block END -->

<!-- Facts block BEGIN -->
<div class="facts-block content content-center" id="facts-block">
    <h2>Прогрессивные Технологии Бизнеса в цифрах</h2>

    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-3 col-xs-6">
                <div class="item">
                    <strong>39</strong>
                    Проектов
                </div>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-6">
                <div class="item">
                    <strong>12</strong>
                    Коллектив
                </div>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-6">
                <div class="item">
                    <strong>190</strong>
                    Консультаций
                </div>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-6">
                <div class="item">
                    <strong>267</strong>
                    Обучено
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Facts block END -->


<!-- Testimonials block BEGIN -->
<div class="testimonials-block content content-center margin-bottom-65" id="feedback">
    <div class="container">
        <h2>Философия<strong> нашей работы</strong></h2>
        <h4>«Встречают по одежке, а провожают по уму»</h4>

        <div class="carousel slide" data-ride="carousel" id="testimonials-block">
            <!-- Wrapper for slides -->
            <div class="carousel-inner">
                <!-- Carousel items -->
                <? foreach ($reviews as $key => $review): ?>
                    <div class="<?= $key == 0 ? "active" : "" ?> item">
                        <blockquote><p><?= $review->text ?></p></blockquote>
                        <span class="testimonials-name"><?= $review->from_company ?></span>
                    </div>
                <? endforeach; ?>
            </div>
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <li data-target="#testimonials-block" data-slide-to="0" class="active"></li>
                <li data-target="#testimonials-block" data-slide-to="1"></li>
                <li data-target="#testimonials-block" data-slide-to="2"></li>
            </ol>
        </div>
        <hr>
        <div class="container">

            <p>Главным для нас является то, насколько полезна будет наша работа клиенту. Чем выше польза для клиента,
                тем более успешным мы считаем результат. Немаловажно и отношение конечного пользователя
                разработки.</p>

            <p>Да, зачастую люди изначально негативно воспринимают перемены, потому что любое новшество выводит из
                зоны комфорта и заставляет человека приспосабливаться. Наибольшие трудности в процессе внедрения
                испытывает конечный пользователь.</p>

            <p>По мере использования нашей разработки человек, вновь возвращаясь в привычную среду, понимает, сколь
                полезную работу мы проделали и сколь безупречно решены поставленные задачи.</p>

            <p>Первоначально мы проповедуем «философию теоретической системы» (или <span>«философия ума»</span>), мы
                не рассчитываем на всестороннюю поддержку пользователя, понимая с какими трудностями он сталкивается,
                и, помогая ему в наименьший срок вернуться в рабочий режим. Главное на данном этапе, чтобы
                реализованная система решала задачи, поставленные клиентом и поддержка руководства.</p>

            <p>Далее мы проповедуем «философию практического применения» (или «философия одежки»), в рамках которой
                решаем вопросы юзабилити и скорости работы конечного пользования и взаимодействия системы с окружающим
                миром. Здесь же мы выясняем, насколько конечный пользователь удовлетворен изменениями и насколько
                практический результат соответствует ожиданиям руководства.</p>

        </div>
    </div>
</div>
<!-- Testimonials block END -->


<!-- Partners block BEGIN -->
<section style="overflow: hidden">
    <div class="partners-block col-md-12">
        <div class="container">
            <div class="row">
                <? foreach ($clients as $client): ?>
                    <div class="col-md-2 col-sm-3 col-xs-6">
                        <div class="thumbnail">
                            <img src="<?= $client->image ?>" alt="<?= $client->name ?>">
                        </div>
                    </div>
                <? endforeach; ?>
            </div>
        </div>
    </div>
</section>
<!-- Partners block END -->

<!-- BEGIN PRE-FOOTER -->
<div class="pre-footer" id="contact">
    <div class="container">
        <div class="row">
            <!-- BEGIN BOTTOM ABOUT BLOCK -->
            <div class="col-md-4 col-sm-6 pre-footer-col">
                <h2>Регионы</h2>

                <p>Наши клиенты ведут свой бизнес в различных городах России. География наших внедрений широка:
                    Центральный федеральный округ, Южный федеральный округ, Сибирский федеральный округ, Уральский
                    федеральный округ, Приволжский федеральный округ.</p>

                <p>Мы не ограничиваемся работой через интернет, готовы приехать и работать на вашей территории.</p>
            </div>
            <!-- END BOTTOM ABOUT BLOCK -->

            <!-- BEGIN TIMEWORK BLOCK -->
            <div class="col-md-4 col-sm-6 pre-footer-col">
                <h2>График работы</h2>
                <b>Понедельник: </b>8:30 - 18:00<br>
                <b>Вторник: </b>8:30 - 18:00<br>
                <b>Среда: </b>8:30 - 18:00<br>
                <b>Четврерг: </b>8:30 - 18:00<br>
                <b>Пятница: </b>8:30 - 16:00<br>
                <b>Суббота, Воскресенье: </b><span>Выходной</span><br><br>

                <p>Указано местное время.<br>Для Москвы +02:00<br>GMT +06:00</p>
            </div>
            <!-- END TIMEWORK BLOCK -->

            <div class="col-md-4 col-sm-6 pre-footer-col">
                <!-- BEGIN BOTTOM CONTACTS -->
                <h2>Наши Контакты</h2>
                <address class="margin-bottom-20">
                    450006, Республика Башкортостан<br>
                    г. Уфа, ул. Ленина, 99, офис 12/1<br>
                    Телефон: +7 (347) 289-91-60<br>
                    Факс: +7 (347) 289-91-91<br>
                    Email: <a href="mailto:company@progtb.ru">company@progtb.ru</a><br>
                    Skype: <a href="skype:progtb">progtb</a>
                </address>
                <!-- END BOTTOM CONTACTS -->

                <div class="pre-footer-subscribe-box">
                    <h2>Новости</h2>

                    <form action="javascript:void(0);">
                        <div class="input-group">
                            <input type="text" placeholder="адрес эл.почты" class="form-control">
                <span class="input-group-btn">
                  <button class="btn btn-primary" type="submit">Подписаться</button>
                </span>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END PRE-FOOTER -->

<!-- BEGIN FOOTER -->
<div class="footer">
    <div class="container">
        <div class="row">
            <!-- BEGIN COPYRIGHT -->
            <div class="col-md-6 col-sm-6">
                <div class="copyright"><?php echo date('Y'); ?> © Прогрессивные Технологии Бизнеса.</div>
            </div>
            <!-- END COPYRIGHT -->
            <!-- BEGIN SOCIAL ICONS
            <div class="col-md-6 col-sm-6 pull-right">
              <ul class="social-icons">
                <li><a class="rss" data-original-title="rss" href="javascript:void(0);"></a></li>
                <li><a class="twitter" data-original-title="twitter" href="javascript:void(0);"></a></li>
                <li><a class="googleplus" data-original-title="googleplus" href="javascript:void(0);"></a></li>
                <li><a class="linkedin" data-original-title="linkedin" href="javascript:void(0);"></a></li>
                <li><a class="youtube" data-original-title="youtube" href="javascript:void(0);"></a></li>
                <li><a class="skype" data-original-title="skype" href="javascript:void(0);"></a></li>
              </ul>
            </div>
            <!-- END SOCIAL ICONS -->
        </div>
    </div>
</div>
<!-- END FOOTER -->

<a href="#promo-block" class="go2top scroll"><i class="fa fa-arrow-up"></i></a>

<!--[if lt IE 9]>
<script src="assets/global/plugins/respond.min.js"></script>
<![endif]-->

<!-- Load JavaScripts at the bottom, because it will reduce page load time -->
<!-- Core plugins BEGIN (For ALL pages) -->
<script src="assets/global/plugins/jquery-1.11.0.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<!-- Core plugins END (For ALL pages) -->

<!-- BEGIN RevolutionSlider -->
<script src="assets/global/plugins/slider-revolution-slider/rs-plugin/js/jquery.themepunch.plugins.min.js"
        type="text/javascript"></script>
<script src="assets/global/plugins/slider-revolution-slider/rs-plugin/js/jquery.themepunch.revolution.min.js"
        type="text/javascript"></script>
<script src="assets/global/plugins/slider-revolution-slider/rs-plugin/js/jquery.themepunch.tools.min.js"
        type="text/javascript"></script>
<script src="assets/frontend/onepage/scripts/revo-ini.js" type="text/javascript"></script>
<!-- END RevolutionSlider -->

<!-- Core plugins BEGIN (required only for current page) -->
<script src="assets/global/plugins/fancybox/source/jquery.fancybox.pack.js" type="text/javascript"></script>
<!-- pop up -->
<script src="assets/global/plugins/jquery.easing.js"></script>
<script src="assets/global/plugins/jquery.parallax.js"></script>
<script src="assets/global/plugins/jquery.scrollTo.min.js"></script>
<script src="assets/frontend/onepage/scripts/jquery.nav.js"></script>
<!-- Core plugins END (required only for current page) -->


<script src="assets/global/plugins/countdown/jquery.countdown.js"></script>

<!-- Global js BEGIN -->
<script src="assets/frontend/onepage/scripts/layout.js" type="text/javascript"></script>
<script>
    $(document).ready(function () {
        Layout.init();
    });

    //COUNTDOWN TIMER
    var newYear = new Date();
    newYear = new Date(newYear.getFullYear() + 1, 1 - 1, 1);
    $('#countdown').countdown({until: new Date(2015, 1 - 1, 26)}); // enter event day

    $('#removeCountdown').toggle(
        function () {
            $(this).text('Re-attach');
            $('#defaultCountdown').countdown('destroy');
        },
        function () {
            $(this).text('Remove');
            $('#defaultCountdown').countdown({until: newYear});
        }
    );

</script>

<!-- Global js END -->