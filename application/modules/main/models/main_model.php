<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}
	
	public function get_projects() //Получаем список проектов из базы
	{		
		$query = $this->db->get('projects');
		return $query->result_array();
	}	
	
	public function get_services() //Получаем список услуг из базы
	{		
		$query = $this->db->get('our_services');
		return $query->result_array();
	}	
}