<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends MX_Controller
{


    public function __construct()
    {
        parent::__construct();

        $this->load->library('Template');
        $this->load->library('events');
        $this->load->library('users/auth');
        $this->load->helper('text');
        // Make the requested page var available, since
        // we're not extending from a Bonfire controller
        // and it's not done for us.

    }

    public function index()
    {
        $this->load->model('main_model'); //Подгружаем модель Main
        Template::set('projects', $this->main_model->get_projects()); //Проекты
        Template::set('services', $this->main_model->get_services()); //Услуги

        //Получаем отзывы
        $this->load->model('reviews/reviews_model');
        $reviews = $this->reviews_model->find_all();
        Template::set('reviews', $reviews);


        //Последние 3 записи блога
        $this->load->model('blog/blog_model');
        $blog = $this->blog_model->order_by("created_on", "desc")->limit(3)->where("publication", 1)->find_all();
        Template::set('blog', $blog);

        //Получаем партнеров
        $this->load->model('clients_partners/clients_partners_model');
        $partners = $this->clients_partners_model->where("partner", 1)->find_all();
        Template::set('partners', $partners);

        //Клиенты
        $clients = $this->clients_partners_model->where("partner ", null)->find_all();
        Template::set('clients', $clients);


        Template::render();

    }
}