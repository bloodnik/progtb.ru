<?Template::block("header_nav_block")?>

<div class="main">
    <div class="container">
        <!-- BEGIN SIDEBAR & CONTENT -->
        <div class="row margin-bottom-40">
            <!-- BEGIN CONTENT -->
            <div class="col-md-12 col-sm-12">
                <div class="content-page page-404">
                    <div class="number">
                        404
                    </div>
                    <div class="details">
                        <h3>Упс!  Вы потерялись.</h3>
                        <p>
                            Мы не можем отобразить для Вас эту страницу.<br>
                            <a href="index.html" class="link">Вернуться на главную</a>.
                        </p>
                    </div>
                </div>
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END SIDEBAR & CONTENT -->
    </div>
</div>

<?Template::block("footer_block")?>