<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class my404 extends Front_Controller
{
    public function index()
    {
        $this->load->library('users/auth');

        $this->output->set_status_header('404');
        Template::set('title', 'Упс, страница не найдена / Блог - ООО "Прогтехбизнес"');

        Template::set_theme('blog', 'default');

        Template::set_block('header_nav_block', '_header_nav_block'); //Установка хедера
        Template::set_block('footer_block', '_footer_block'); //Установка футера



        Template::render();
    }
}
