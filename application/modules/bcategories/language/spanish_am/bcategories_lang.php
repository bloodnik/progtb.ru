<?php defined('BASEPATH') || exit('No direct script access allowed');

$lang['bcategories_manage']      = 'Gestionar Bcategories';
$lang['bcategories_edit']        = 'Editar';
$lang['bcategories_true']        = 'Verdadero';
$lang['bcategories_false']       = 'Falso';
$lang['bcategories_create']      = 'Crear';
$lang['bcategories_list']        = 'Listar';
$lang['bcategories_new']       = 'Nuevo';
$lang['bcategories_edit_text']     = 'Editar esto para satisfacer sus necesidades';
$lang['bcategories_no_records']    = 'Hay ninguna bcategories en la sistema.';
$lang['bcategories_create_new']    = 'Crear nuevo(a) Bcategories.';
$lang['bcategories_create_success']  = 'Bcategories creado(a) con éxito.';
$lang['bcategories_create_failure']  = 'Hubo un problema al crear el(la) bcategories: ';
$lang['bcategories_create_new_button'] = 'Crear nuevo(a) Bcategories';
$lang['bcategories_invalid_id']    = 'ID de Bcategories inválido(a).';
$lang['bcategories_edit_success']    = 'Bcategories guardado correctamente.';
$lang['bcategories_edit_failure']    = 'Hubo un problema guardando el(la) bcategories: ';
$lang['bcategories_delete_success']  = 'Registro(s) eliminado con éxito.';
$lang['bcategories_delete_failure']  = 'No hemos podido eliminar el registro: ';
$lang['bcategories_delete_error']    = 'No ha seleccionado ning&#250;n registro que desea eliminar.';
$lang['bcategories_actions']     = 'Açciones';
$lang['bcategories_cancel']      = 'Cancelar';
$lang['bcategories_delete_record']   = 'Eliminar este(a) Bcategories';
$lang['bcategories_delete_confirm']  = '¿Esta seguro de que desea eliminar este(a) bcategories?';
$lang['bcategories_edit_heading']    = 'Editar Bcategories';

// Create/Edit Buttons
$lang['bcategories_action_edit']   = 'Guardar Bcategories';
$lang['bcategories_action_create']   = 'Crear Bcategories';

// Activities
$lang['bcategories_act_create_record'] = 'Creado registro con ID';
$lang['bcategories_act_edit_record'] = 'Actualizado registro con ID';
$lang['bcategories_act_delete_record'] = 'Eliminado registro con ID';

//Listing Specifics
$lang['bcategories_records_empty']    = 'No hay registros encontrados para su selección.';
$lang['bcategories_errors_message']    = 'Por favor corrija los siguientes errores:';

// Column Headings
$lang['bcategories_column_created']  = 'Creado';
$lang['bcategories_column_deleted']  = 'Elíminado';
$lang['bcategories_column_modified'] = 'Modificado';

// Module Details
$lang['bcategories_module_name'] = 'Bcategories';
$lang['bcategories_module_description'] = 'Список категорий блога';
$lang['bcategories_area_title'] = 'Bcategories';

// Fields
$lang['bcategories_field_name'] = 'Name';
