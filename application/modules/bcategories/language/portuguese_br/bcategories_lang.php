<?php defined('BASEPATH') || exit('No direct script access allowed');

$lang['bcategories_manage']      = 'Gerenciar Bcategories';
$lang['bcategories_edit']        = 'Editar';
$lang['bcategories_true']        = 'Verdadeiro';
$lang['bcategories_false']       = 'Falso';
$lang['bcategories_create']      = 'Criar';
$lang['bcategories_list']        = 'Listar';
$lang['bcategories_new']       = 'Novo';
$lang['bcategories_edit_text']     = 'Edite isto conforme sua necessidade';
$lang['bcategories_no_records']    = 'Não há bcategories no sistema.';
$lang['bcategories_create_new']    = 'Criar novo(a) Bcategories.';
$lang['bcategories_create_success']  = 'Bcategories Criado(a) com sucesso.';
$lang['bcategories_create_failure']  = 'Ocorreu um problema criando o(a) bcategories: ';
$lang['bcategories_create_new_button'] = 'Criar novo(a) Bcategories';
$lang['bcategories_invalid_id']    = 'ID de Bcategories inválida.';
$lang['bcategories_edit_success']    = 'Bcategories salvo(a) com sucesso.';
$lang['bcategories_edit_failure']    = 'Ocorreu um problema salvando o(a) bcategories: ';
$lang['bcategories_delete_success']  = 'Registro(s) excluído(s) com sucesso.';
$lang['bcategories_delete_failure']  = 'Não foi possível excluir o registro: ';
$lang['bcategories_delete_error']    = 'Voc6e não selecionou nenhum registro para excluir.';
$lang['bcategories_actions']     = 'Ações';
$lang['bcategories_cancel']      = 'Cancelar';
$lang['bcategories_delete_record']   = 'Excluir este(a) Bcategories';
$lang['bcategories_delete_confirm']  = 'Você tem certeza que deseja excluir este(a) bcategories?';
$lang['bcategories_edit_heading']    = 'Editar Bcategories';

// Create/Edit Buttons
$lang['bcategories_action_edit']   = 'Salvar Bcategories';
$lang['bcategories_action_create']   = 'Criar Bcategories';

// Activities
$lang['bcategories_act_create_record'] = 'Criado registro com ID';
$lang['bcategories_act_edit_record'] = 'Atualizado registro com ID';
$lang['bcategories_act_delete_record'] = 'Excluído registro com ID';

//Listing Specifics
$lang['bcategories_records_empty']    = 'Nenhum registro encontrado.';
$lang['bcategories_errors_message']    = 'Por favor corrija os erros a seguir:';

// Column Headings
$lang['bcategories_column_created']  = 'Criado';
$lang['bcategories_column_deleted']  = 'Excluído';
$lang['bcategories_column_modified'] = 'Atualizado';

// Module Details
$lang['bcategories_module_name'] = 'Bcategories';
$lang['bcategories_module_description'] = 'Список категорий блога';
$lang['bcategories_area_title'] = 'Bcategories';

// Fields
$lang['bcategories_field_name'] = 'Name';
