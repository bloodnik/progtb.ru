<?php defined('BASEPATH') || exit('No direct script access allowed');

$lang['bcategories_manage']      = 'Gestisci Bcategories';
$lang['bcategories_edit']        = 'Modifica';
$lang['bcategories_true']        = 'Vero';
$lang['bcategories_false']       = 'Falso';
$lang['bcategories_create']      = 'Crea';
$lang['bcategories_list']        = 'Elenca';
$lang['bcategories_new']       = 'Nuovo';
$lang['bcategories_edit_text']     = 'Modifica questo secondo le tue necessità';
$lang['bcategories_no_records']    = 'Non ci sono bcategories nel sistema.';
$lang['bcategories_create_new']    = 'Crea un nuovo Bcategories.';
$lang['bcategories_create_success']  = 'Bcategories creato con successo.';
$lang['bcategories_create_failure']  = 'C\'è stato un problema nella creazione di bcategories: ';
$lang['bcategories_create_new_button'] = 'Crea nuovo Bcategories';
$lang['bcategories_invalid_id']    = 'ID Bcategories non valido.';
$lang['bcategories_edit_success']    = 'Bcategories creato con successo.';
$lang['bcategories_edit_failure']    = 'C\'è stato un errore nel salvataggio di bcategories: ';
$lang['bcategories_delete_success']  = 'record(s) creati con successo.';
$lang['bcategories_delete_failure']  = 'Non possiamo eliminare il record: ';
$lang['bcategories_delete_error']    = 'Non hai selezionato alcun record da eliminare.';
$lang['bcategories_actions']     = 'Azioni';
$lang['bcategories_cancel']      = 'Cancella';
$lang['bcategories_delete_record']   = 'Elimina questo Bcategories';
$lang['bcategories_delete_confirm']  = 'Sei sicuro di voler eliminare questo bcategories?';
$lang['bcategories_edit_heading']    = 'Modifica Bcategories';

// Create/Edit Buttons
$lang['bcategories_action_edit']   = 'Salva Bcategories';
$lang['bcategories_action_create']   = 'Crea Bcategories';

// Activities
$lang['bcategories_act_create_record'] = 'Creato il record con ID';
$lang['bcategories_act_edit_record'] = 'Aggiornato il record con ID';
$lang['bcategories_act_delete_record'] = 'Eliminato il record con ID';

// Listing Specifics
$lang['bcategories_records_empty']    = 'Nessun record trovato che corrisponda alla tua selezione.';
$lang['bcategories_errors_message']    = 'Per favore risolvi i seguenti errori:';

// Column Headings
$lang['bcategories_column_created']  = 'Creato';
$lang['bcategories_column_deleted']  = 'Eliminato';
$lang['bcategories_column_modified'] = 'Modificato';

// Module Details
$lang['bcategories_module_name'] = 'Bcategories';
$lang['bcategories_module_description'] = 'Список категорий блога';
$lang['bcategories_area_title'] = 'Bcategories';

// Fields
$lang['bcategories_field_name'] = 'Name';
