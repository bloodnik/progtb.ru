<?php defined('BASEPATH') || exit('No direct script access allowed');


$lang['bcategories_manage']      = 'Управление категориями';
$lang['bcategories_edit']        = 'Редактировать';
$lang['bcategories_true']        = 'Да';
$lang['bcategories_false']       = 'Нет';
$lang['bcategories_create']      = 'Создать';
$lang['bcategories_list']        = 'Список категорий';
$lang['bcategories_new']       = 'Новый элемент';
$lang['bcategories_edit_text']     = 'Отредактируте элемент если вам это необходимо';
$lang['bcategories_no_records']    = 'Нет ни одной записи.';
$lang['bcategories_create_new']    = 'Добавить новый элемент.';
$lang['bcategories_create_success']  = 'Элемент успешно добавлен.';
$lang['bcategories_create_failure']  = 'Есть некоторые проблемы при создании: ';
$lang['bcategories_create_new_button'] = 'Создать новые элементы';
$lang['bcategories_invalid_id']    = 'Неверный ID элемента.';
$lang['bcategories_edit_success']    = 'Элемент успешно сохранен.';
$lang['bcategories_edit_failure']    = 'Есть проблемы при сохранении: ';
$lang['bcategories_delete_success']  = 'Запись(и) успешно удалена.';
$lang['bcategories_delete_failure']  = 'Не возможно удалить элемент: ';
$lang['bcategories_delete_error']    = 'Не выбран ни один элемент для удаления.';
$lang['bcategories_actions']     = 'Действия';
$lang['bcategories_cancel']      = 'Отмена';
$lang['bcategories_delete_record']   = 'Удалить этот элемент';
$lang['bcategories_delete_confirm']  = 'Вы действительно хотите удалить этот элемент?';
$lang['bcategories_edit_heading']    = 'Редактирование элемента';

// Create/Edit Buttons
$lang['bcategories_action_edit']   = 'Сохранить элемент';
$lang['bcategories_action_create']   = 'Создать элемент';

// Activities
$lang['bcategories_act_create_record'] = 'Создать запись с ID';
$lang['bcategories_act_edit_record'] = 'Обновить запись с ID';
$lang['bcategories_act_delete_record'] = 'Удалить запись с ID';

//Listing Specifics
$lang['bcategories_records_empty']    = 'Не найдены удовлетворяющие вас записи.';
$lang['bcategories_errors_message']    = 'Пожалуйста, исправте следующие проблемы:';

// Column Headings
$lang['bcategories_column_created']  = 'Создано';
$lang['bcategories_column_deleted']  = 'Удалено';
$lang['bcategories_column_modified'] = 'Обновлено';
$lang['bcategories_column_deleted_by'] = 'Удалено ';
$lang['bcategories_column_created_by'] = 'Создано';
$lang['bcategories_column_modified_by'] = 'Обновлено';

// Module Details
$lang['bcategories_module_name'] = 'Элементы';
$lang['bcategories_module_description'] = 'Категории';
$lang['bcategories_area_title'] = 'Категории блога';


// Fields
$lang['bcategories_field_name'] = 'Название';