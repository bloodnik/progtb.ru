<?php defined('BASEPATH') || exit('No direct script access allowed');


$lang['bcategories_manage']      = 'Manage Bcategories';
$lang['bcategories_edit']        = 'Edit';
$lang['bcategories_true']        = 'True';
$lang['bcategories_false']       = 'False';
$lang['bcategories_create']      = 'Create';
$lang['bcategories_list']        = 'List';
$lang['bcategories_new']       = 'New';
$lang['bcategories_edit_text']     = 'Edit this to suit your needs';
$lang['bcategories_no_records']    = 'There are no bcategories in the system.';
$lang['bcategories_create_new']    = 'Create a new Bcategories.';
$lang['bcategories_create_success']  = 'Bcategories successfully created.';
$lang['bcategories_create_failure']  = 'There was a problem creating the bcategories: ';
$lang['bcategories_create_new_button'] = 'Create New Bcategories';
$lang['bcategories_invalid_id']    = 'Invalid Bcategories ID.';
$lang['bcategories_edit_success']    = 'Bcategories successfully saved.';
$lang['bcategories_edit_failure']    = 'There was a problem saving the bcategories: ';
$lang['bcategories_delete_success']  = 'record(s) successfully deleted.';
$lang['bcategories_delete_failure']  = 'We could not delete the record: ';
$lang['bcategories_delete_error']    = 'You have not selected any records to delete.';
$lang['bcategories_actions']     = 'Actions';
$lang['bcategories_cancel']      = 'Cancel';
$lang['bcategories_delete_record']   = 'Delete this Bcategories';
$lang['bcategories_delete_confirm']  = 'Are you sure you want to delete this bcategories?';
$lang['bcategories_edit_heading']    = 'Edit Bcategories';

// Create/Edit Buttons
$lang['bcategories_action_edit']   = 'Save Bcategories';
$lang['bcategories_action_create']   = 'Create Bcategories';

// Activities
$lang['bcategories_act_create_record'] = 'Created record with ID';
$lang['bcategories_act_edit_record'] = 'Updated record with ID';
$lang['bcategories_act_delete_record'] = 'Deleted record with ID';

//Listing Specifics
$lang['bcategories_records_empty']    = 'No records found that match your selection.';
$lang['bcategories_errors_message']    = 'Please fix the following errors:';

// Column Headings
$lang['bcategories_column_created']  = 'Created';
$lang['bcategories_column_deleted']  = 'Deleted';
$lang['bcategories_column_modified'] = 'Modified';
$lang['bcategories_column_deleted_by'] = 'Deleted By';
$lang['bcategories_column_created_by'] = 'Created By';
$lang['bcategories_column_modified_by'] = 'Modified By';

// Module Details
$lang['bcategories_module_name'] = 'Bcategories';
$lang['bcategories_module_description'] = 'Список категорий блога';
$lang['bcategories_area_title'] = 'Bcategories';

// Fields
$lang['bcategories_field_name'] = 'Name';