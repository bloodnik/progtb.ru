<?php defined('BASEPATH') || exit('No direct script access allowed');

/**
 * Content controller
 */
class Content extends Admin_Controller
{
    protected $permissionCreate = 'Bcategories.Content.Create';
    protected $permissionDelete = 'Bcategories.Content.Delete';
    protected $permissionEdit   = 'Bcategories.Content.Edit';
    protected $permissionView   = 'Bcategories.Content.View';

    /**
	 * Constructor
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
		
        $this->auth->restrict($this->permissionView);
		$this->load->model('bcategories/bcategories_model');
        $this->lang->load('bcategories');

            $this->form_validation->set_error_delimiters("<span class='error'>", "</span>");
        
		Template::set_block('sub_nav', 'content/_sub_nav');

		Assets::add_module_js('bcategories', 'bcategories.js');
	}

	/**
	 * Display a list of Bcategories data.
	 *
	 * @return void
	 */
	public function index($offset = 0)
	{
        // Deleting anything?
		if (isset($_POST['delete'])) {
            $this->auth->restrict($this->permissionDelete);
			$checked = $this->input->post('checked');
			if (is_array($checked) && count($checked)) {

                // If any of the deletions fail, set the result to false, so
                // failure message is set if any of the attempts fail, not just
                // the last attempt

				$result = true;
				foreach ($checked as $pid) {
					$deleted = $this->bcategories_model->delete($pid);
                    if ($deleted == false) {
                        $result = false;
                    }
				}
				if ($result) {
					Template::set_message(count($checked) . ' ' . lang('bcategories_delete_success'), 'success');
				} else {
					Template::set_message(lang('bcategories_delete_failure') . $this->bcategories_model->error, 'error');
				}
			}
		}
        $pagerUriSegment = 5;
        $pagerBaseUrl = site_url(SITE_AREA . '/content/bcategories/index') . '/';
        
        $limit  = $this->settings_lib->item('site.list_limit') ?: 15;

        $this->load->library('pagination');
        $pager['base_url']    = $pagerBaseUrl;
        $pager['total_rows']  = $this->bcategories_model->count_all();
        $pager['per_page']    = $limit;
        $pager['uri_segment'] = $pagerUriSegment;

        $this->pagination->initialize($pager);
        $this->bcategories_model->limit($limit, $offset);
        
		$records = $this->bcategories_model->find_all();

		Template::set('records', $records);
        
    Template::set('toolbar_title', lang('bcategories_manage'));

		Template::render();
	}
    
    /**
	 * Create a Bcategories object.
	 *
	 * @return void
	 */
	public function create()
	{
		$this->auth->restrict($this->permissionCreate);

		if (isset($_POST['save'])) {
			if ($insert_id = $this->save_bcategories()) {
				log_activity($this->auth->user_id(), lang('bcategories_act_create_record') . ': ' . $insert_id . ' : ' . $this->input->ip_address(), 'bcategories');
				Template::set_message(lang('bcategories_create_success'), 'success');

				redirect(SITE_AREA . '/content/bcategories');
			}

            // Not validation error
			if ( ! empty($this->bcategories_model->error)) {
				Template::set_message(lang('bcategories_create_failure') . $this->bcategories_model->error, 'error');
            }
		}

		Template::set('toolbar_title', lang('bcategories_action_create'));

		Template::render();
	}
	/**
	 * Allows editing of Bcategories data.
	 *
	 * @return void
	 */
	public function edit()
	{
		$id = $this->uri->segment(5);
		if (empty($id)) {
			Template::set_message(lang('bcategories_invalid_id'), 'error');

			redirect(SITE_AREA . '/content/bcategories');
		}
        
		if (isset($_POST['save'])) {
			$this->auth->restrict($this->permissionEdit);

			if ($this->save_bcategories('update', $id)) {
				log_activity($this->auth->user_id(), lang('bcategories_act_edit_record') . ': ' . $id . ' : ' . $this->input->ip_address(), 'bcategories');
				Template::set_message(lang('bcategories_edit_success'), 'success');
				redirect(SITE_AREA . '/content/bcategories');
			}

            // Not validation error
            if ( ! empty($this->bcategories_model->error)) {
                Template::set_message(lang('bcategories_edit_failure') . $this->bcategories_model->error, 'error');
			}
		}
        
		elseif (isset($_POST['delete'])) {
			$this->auth->restrict($this->permissionDelete);

			if ($this->bcategories_model->delete($id)) {
				log_activity($this->auth->user_id(), lang('bcategories_act_delete_record') . ': ' . $id . ' : ' . $this->input->ip_address(), 'bcategories');
				Template::set_message(lang('bcategories_delete_success'), 'success');

				redirect(SITE_AREA . '/content/bcategories');
			}

            Template::set_message(lang('bcategories_delete_failure') . $this->bcategories_model->error, 'error');
		}
        
        Template::set('bcategories', $this->bcategories_model->find($id));

		Template::set('toolbar_title', lang('bcategories_edit_heading'));
		Template::render();
	}

	//--------------------------------------------------------------------
	// !PRIVATE METHODS
	//--------------------------------------------------------------------

	/**
	 * Save the data.
	 *
	 * @param string $type Either 'insert' or 'update'.
	 * @param int	 $id	The ID of the record to update, ignored on inserts.
	 *
	 * @return bool|int An int ID for successful inserts, true for successful
     * updates, else false.
	 */
	private function save_bcategories($type = 'insert', $id = 0)
	{
		if ($type == 'update') {
			$_POST['id'] = $id;
		}

        // Validate the data
        $this->form_validation->set_rules($this->bcategories_model->get_validation_rules());
        if ($this->form_validation->run() === false) {
            return false;
        }

		// Make sure we only pass in the fields we want
		
		$data = $this->bcategories_model->prep_data($this->input->post());

        // Additional handling for default values should be added below,
        // or in the model's prep_data() method


        $return = false;
		if ($type == 'insert') {
			$id = $this->bcategories_model->insert($data);

			if (is_numeric($id)) {
				$return = $id;
			}
		} elseif ($type == 'update') {
			$return = $this->bcategories_model->update($id, $data);
		}

		return $return;
	}
}