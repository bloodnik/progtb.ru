<?php defined('BASEPATH') || exit('No direct script access allowed');

/**
 * Blog controller
 */
class Blog extends Front_Controller
{
    protected $permissionCreate = 'Blog.Blog.Create';
    protected $permissionDelete = 'Blog.Blog.Delete';
    protected $permissionEdit   = 'Blog.Blog.Edit';
    protected $permissionView   = 'Blog.Blog.View';
    public $is_admin = false;

    /**
	 * Constructor
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
        $this->load->library('users/auth');
		$this->load->model('blog/blog_model');
        $this->load->model('bcategories/bcategories_model'); //Подгружаем модуль категорий
        $this->lang->load('blog');
        $this->load->helper('text');

        if($this->auth->has_permission('Blog.Content.Edit', $this->auth->role_id())){
            $this->is_admin = true;
        }

		Assets::add_module_js('blog', 'blog.js');
		Assets::add_module_css('blog', 'blog.css');

        Template::set_theme('blog', 'default');

        Template::set_block('header_nav_block', '_header_nav_block'); //Установка хедера
        Template::set_block('footer_block', '_footer_block'); //Установка футера

	}

	/**
	 * Display a list of Blog data.
	 *
	 * @return void
	 */
	public function index($offset = 0)
	{

        $pagerUriSegment = 3;
        $pagerBaseUrl = site_url('blog/index') . '/';
        
        $limit  = $this->settings_lib->item('site.list_limit') ?: 15;

        $this->load->library('pagination');
        $pager['base_url']    = $pagerBaseUrl;
        $pager['total_rows']  = $this->blog_model->count_all();
        $pager['per_page']    = $limit;
        $pager['uri_segment'] = $pagerUriSegment;

        $this->pagination->initialize($pager);
        $this->blog_model->limit($limit, $offset);

        self::get_sidebar_info(); //Получаем информацию для сайдбара

        //Получамем записи блога
        $category_id = isset($_REQUEST["cat"])?$_REQUEST["cat"]:"";
        $this->blog_model->where($this->blog_model->get_deleted_field(), 0);

        if($category_id == 100){ //Если есть доступ к закрытому разделу
                $posts = $this->blog_model->order_by("created_on", "desc")->where(array("role_id" => $this->auth->role_id(), "publication" => 1))->find_all();
        }elseif(strlen($category_id)>0){
            if ($this->is_admin) {
                $posts = $this->blog_model->order_by("created_on", "desc")->where(array("category" => $category_id))->find_all();
            }
            else{
                $posts = $this->blog_model->order_by("created_on", "desc")->where(array("category" => $category_id, "role_id" => 4, "publication" => 1))->find_all();
            }
        }
        else{
            if ($this->is_admin) {
                $posts = $this->blog_model->order_by("created_on", "desc")->find_all();
            }
            else{
                $posts = $this->blog_model->order_by("created_on", "desc")->where(array("role_id" => 4, "publication" => 1))->find_all();
            }
            //Поиск по тегам
            $tag_search = isset($_REQUEST["tag"])?$_REQUEST["tag"]:"";
            if(strlen($tag_search)>0){
                foreach ($posts as $key => $post) {
                    if (!is_numeric(strpos($post->tags, $tag_search)) && !strpos($post->tags, $tag_search)>= 0){
                        unset($posts[$key]);
                    }
                }
            }
        }

        if (isset($posts) && is_array($posts) && count($posts)) :
            foreach ($posts as $key => $post) {
                $posts[$key]->tags = (explode(", ", $post->tags));
            }
        endif;


        Template::set('posts', $posts);

        Template::set('title', 'Блог - ООО "Прогтехбизнес"');

		Template::render();
	}

    public function detail($code=0) {

        self::get_sidebar_info(); //Получаем информацию для сайдбара

        $post = $this->blog_model->find_by('code', $code);

        if (!is_object($post)){
            $error = "Запись не найдена";
            Template::set('error', "Запись не найдена");
            Template::set('title', $post->name);
        }
        else{
            Template::set('post', $post);
            Template::set('title', $post->name);
            Template::set('detail_tags',  explode(", ", $post->tags));
        }

        Template::render();
    }

    private function get_sidebar_info(){
        //Получамем записи блога
        $this->blog_model->where($this->blog_model->get_deleted_field(), 0);
        $records = $this->blog_model->order_by("created_on", "desc")->limit(5)->where("publication" , 1)->find_all();
        Template::set('records', $records);

        //Заполняем массив категорий и тегов
        $categories = array();
        $tags = "";
        foreach($records as $record) {
            $categories[] = intval($record->category);
            $tags .= $record-> tags;
            $tags .= ", ";

        }
        $categories_count = array_count_values($categories); //Подчет количества элементов в категории

        $result = $this->bcategories_model->find_all();//Выборка из категорий
        $categories_name = array();
        foreach ($result as $value) {
            if(array_key_exists($value->id, $categories_count)){
                $categories_name[$value->id] = $value->name;
            }
        }

        //PR(gettype($this->auth->role_id()));

        if(gettype($this->auth->role_id()) == "integer" && $this->auth->role_id() <> 4) {
            $categories_name[100] = "Закрытый раздел";
        }

        unset($categories);
        $categories = array();
        $categories["count"] = $categories_count;
        $categories["names"] = $categories_name;
        Template::set('categories', $categories);
        Template::set_block('category_block', '_category_block'); //Установка блока категорий

        Template::set_block('recent_block', '_recent_block'); //Установка блока последних постов

        $tags = mb_substr($tags, 0, -2); //Вырезаем последние 2 символа ", "
        Template::set('tags', explode(", ", $tags));
        Template::set_block('tags_block', '_tags_block'); //Установка блока тегов
    }

    private function set() {

    }

    
}