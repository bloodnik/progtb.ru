<?php defined('BASEPATH') || exit('No direct script access allowed');

class Migration_Add_publication_column extends Migration
{
    /**
     * Install this version
     *
     * @return void
     */
    public function up()
    {
        $fields = array(
            'publication' => array(
                'type'       => 'TINYINT',
                'constraint' => 1,
                'default'    => '0',
            ),
        );
        $this->dbforge->add_column('blog', $fields, "modified_by");
    }

    /**
     * Uninstall this version
     *
     * @return void
     */
    public function down()
    {
        $this->dbforge->drop_column('blog', "publication");
    }
}