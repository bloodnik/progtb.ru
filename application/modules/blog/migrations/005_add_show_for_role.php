<?php defined('BASEPATH') || exit('No direct script access allowed');

class Migration_Add_show_for_role extends Migration
{
    /**
     * Install this version
     *
     * @return void
     */
    public function up()
    {
        $fields = array(
            'role_id' => array(
                'type'       => 'INT',
                'constraint' => 2,
                'null'       => false,
            ),
        );
        $this->dbforge->add_column('blog', $fields);
    }

    /**
     * Uninstall this version
     *
     * @return void
     */
    public function down()
    {
        $this->dbforge->drop_column('blog', "role_id");
    }
}