<?php defined('BASEPATH') || exit('No direct script access allowed');

class Migration_Add_post_code extends Migration
{
    /**
     * Install this version
     *
     * @return void
     */
    public function up()
    {
        $fields = array(
        'code' => array(
            'type'       => 'VARCHAR',
            'constraint' => 255,
            'null'       => false,
        ),
    );
        $this->dbforge->add_column('blog', $fields, "id");
    }

    /**
     * Uninstall this version
     *
     * @return void
     */
    public function down()
    {
        $this->dbforge->drop_column('blog', "code");
    }
}