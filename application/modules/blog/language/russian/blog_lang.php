<?php defined('BASEPATH') || exit('No direct script access allowed');

$lang['blog_manage']      = 'Управление блогом';
$lang['blog_edit']        = 'Редактировать';
$lang['blog_true']        = 'Да';
$lang['blog_false']       = 'Нет';
$lang['blog_create']      = 'Создать';
$lang['blog_list']        = 'Список постов';
$lang['blog_new']       = 'Новый пост';
$lang['blog_edit_text']     = 'Отредактируте пост если вам это необходимо';
$lang['blog_no_records']    = 'Нет ни одной записи.';
$lang['blog_create_new']    = 'Добавить новый пост.';
$lang['blog_create_success']  = 'Пост успешно добавлен.';
$lang['blog_create_failure']  = 'Есть некоторые проблемы при создании: ';
$lang['blog_create_new_button'] = 'Создать новые посты';
$lang['blog_invalid_id']    = 'Неверный ID поста.';
$lang['blog_edit_success']    = 'Пост успешно сохранен.';
$lang['blog_edit_failure']    = 'Есть проблемы при сохранении: ';
$lang['blog_delete_success']  = 'Запись(и) успешно удалена.';
$lang['blog_delete_failure']  = 'Не возможно удалить пост: ';
$lang['blog_delete_error']    = 'Не выбран ни один пост для удаления.';
$lang['blog_actions']     = 'Действия';
$lang['blog_cancel']      = 'Отмена';
$lang['blog_delete_record']   = 'Удалить этот пост';
$lang['blog_repair_record']   = 'Востановить этот пост';
$lang['blog_delete_confirm']  = 'Вы действительно хотите удалить этот пост?';
$lang['blog_repair_confirm']  = 'Вы действительно хотите востановить этот пост?';

$lang['blog_edit_heading']    = 'Редактирование поста';
$lang['blog_action_repair']    = 'Восстановить';
$lang['blog_repair_confirm']    = 'Вы действительно хотите восстановить эти записи';




// Create/Edit Buttons
$lang['blog_action_edit']   = 'Сохранить пост';
$lang['blog_action_create']   = 'Создать пост';

// Activities
$lang['blog_act_create_record'] = 'Создать запись с ID';
$lang['blog_act_edit_record'] = 'Обновить запись с ID';
$lang['blog_act_delete_record'] = 'Удалить запись с ID';

//Listing Specifics
$lang['blog_records_empty']    = 'Не найдены удовлетворяющие вас записи.';
$lang['blog_errors_message']    = 'Пожалуйста, исправте следующие проблемы:';

// Column Headings
$lang['blog_column_created']  = 'Создано';
$lang['blog_column_deleted']  = 'Удалено';
$lang['blog_column_modified'] = 'Обновлено';
$lang['blog_column_deleted_by'] = 'Удалено ';
$lang['blog_column_created_by'] = 'Создано';
$lang['blog_column_modified_by'] = 'Обновлено';

// Module Details
$lang['blog_module_name'] = 'Посты';
$lang['blog_module_description'] = 'Наши посты';
$lang['blog_area_title'] = 'Посты';


// Fields
$lang['blog_field_name'] = 'Заголовок';
$lang['blog_field_code'] = 'Символьный код';
$lang['blog_field_publication'] = 'Опубликовано';
$lang['blog_field_text'] = 'Основной текст';
$lang['blog_field_category'] = 'Категория';
$lang['blog_field_image'] = 'Изображение';
$lang['blog_field_tags'] = 'Тэги';
$lang['blog_field_show_count'] = 'Количество просмотров';
$lang['blog_column_vk_posting'] = 'Опубликовать в ВК';
$lang['blog_field_vk_posting'] = 'Опубликовать';