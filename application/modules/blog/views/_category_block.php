<!-- CATEGORIES START -->
<h2 class="no-top-space">Категории</h2>

<ul class="nav sidebar-categories margin-bottom-40">
    <?foreach($categories["names"] as $key => $name):?>
        <li><a href="/blog?cat=<?=$key?>"><?=$name?>
                <?if(isset($categories["count"][intval($key)])):?>
                    (<?=$categories["count"][intval($key)]?>)
                <?endif?>
        </a></li>
    <?endforeach;?>
</ul>
<!-- CATEGORIES END -->