<!-- BEGIN BLOG TAGS -->
<div class="blog-tags margin-bottom-20">
    <h2>Тэги</h2>
    <ul>
        <?foreach ($tags as $tag):?>
            <li><a href="/blog?tag=<?=$tag?>"><i class="fa fa-tags"></i><?=$tag?></a></li>
        <?endforeach;?>
    </ul>
</div>
<!-- END BLOG TAGS -->