<?Template::block("header_nav_block")?>

<div class="main">
<div class="container">
<ul class="breadcrumb">
    <li><a href="/">Главная</a></li>
    <li><a href="/blog">Блог</a></li>
</ul>
<!-- BEGIN SIDEBAR & CONTENT -->
<div class="row margin-bottom-40">
<!-- BEGIN CONTENT -->
<div class="col-md-12 col-sm-12">
<h1>Блог компании</h1>
<div class="content-page">
<div class="row">

<!-- BEGIN LEFT SIDEBAR -->
<div class="col-md-9 col-sm-9 blog-posts">
    <?php if (isset($posts) && is_array($posts) && count($posts)) : ?>
        <?//print_r($posts);?>

            <?foreach ($posts as $post) :?>

                <div class="row">
                    <div class="col-md-4 col-sm-4">
                        <img class="img-responsive" alt="" src="<?=$post->image?>">
                    </div>
                    <div class="col-md-8 col-sm-8">
                        <h2><a href="/blog/detail/<?=$post->code?>"><?=$post->name?></a>
                            <?if(!$post->publication):?>
                                <span class="badge badge-primary">Черновик</span>
                            <?endif?>
                        </h2>
                        <ul class="blog-info">
                            <li><i class="fa fa-calendar"></i> <?=$post->created_on?></li>
    <!--                        <li><i class="fa fa-comments"></i> 17</li>-->
                            <li><i class="fa fa-tags"></i>
                                <?foreach($post->tags as $tag):?>
                                    <a href="/blog?tag=<?=$tag?>"><?=$tag?></a>,
                                <?endforeach?>
                            </li>
                        </ul>
                        <p><?=word_limiter($post->text, 40)?></p>
                        <a href="/blog/detail/<?=$post->code?>" class="more">Подробнее <i class="icon-angle-right"></i></a>
                    </div>
                </div>
                <hr class="blog-post-sep">
            <?endforeach;?>
            <?echo $this->pagination->create_links();?>
<!--            <ul class="pagination">-->
<!--                <li><a href="#">Пред</a></li>-->
<!--                <li><a href="#">1</a></li>-->
<!--                <li><a href="#">2</a></li>-->
<!--                <li class="active"><a href="#">3</a></li>-->
<!--                <li><a href="#">4</a></li>-->
<!--                <li><a href="#">5</a></li>-->
<!--                <li><a href="#">След</a></li>-->
<!--            </ul>-->
    <?else:?>
        Не найдено ни одной записи
    <?endif;?>
</div>
<!-- END LEFT SIDEBAR -->

<!-- BEGIN RIGHT SIDEBAR -->
<div class="col-md-3 col-sm-3 blog-sidebar">
    <?Template::block('category_block'); //Подключаем блок категорий?>
    <div class="fixed_sidebar">
        <?Template::block('recent_block'); //Подключаем блок последних постов?>
        <?Template::block('tags_block'); //Подключаем блок последних постов?>
    </div>

</div>
<!-- END RIGHT SIDEBAR -->
</div>
</div>
</div>
<!-- END CONTENT -->
</div>
<!-- END SIDEBAR & CONTENT -->
</div>
</div>
<?Template::block("footer_block")?>


<!-- Load javascripts at bottom, this will reduce page load time -->
<!-- BEGIN CORE PLUGINS (REQUIRED FOR ALL PAGES) -->
<!--[if lt IE 9]>
<script src="/assets/global/plugins/respond.min.js"></script>
<![endif]-->
<script src="/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="/assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<script src="/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="/assets/frontend/layout/scripts/back-to-top.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->

<!-- BEGIN PAGE LEVEL JAVASCRIPTS (REQUIRED ONLY FOR CURRENT PAGE) -->
<script src="/assets/global/plugins/fancybox/source/jquery.fancybox.pack.js" type="text/javascript"></script><!-- pop up -->

<?echo Assets::js();?>

<script src="/assets/frontend/layout/scripts/layout.js" type="text/javascript"></script>
<script type="text/javascript">

</script>
<!-- END PAGE LEVEL JAVASCRIPTS -->
</body>

