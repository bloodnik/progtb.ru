<!-- BEGIN RECENT NEWS -->

<div class="recent-news margin-bottom-10">
    <h2>Последние посты</h2>
    <?foreach($records as $record):?>
        <div class="row margin-bottom-10">
            <div class="col-md-3">
                <div class="thumbnail">
                    <img class="img-responsive" alt="<?=$record->name?>" src="<?=$record->image?>">
                </div>
            </div>
            <div class="col-md-9 recent-news-inner">
                <h3><a href="/blog/detail/<?=$record->code?>"><?=$record->name?></a></h3>
                <p><?=$record->created_on?></p>
            </div>
        </div>
    <?endforeach;?>
</div>
<!-- END RECENT NEWS -->