<!-- Body BEGIN -->
<body class="corporate menu-always-on-top">

<!-- BEGIN HEADER -->
<div class="header header-mobi-ext">
    <!-- BEGIN TOP BAR -->
    <div class="pre-header">
        <div class="container">
            <div class="row">
                <!-- BEGIN TOP BAR LEFT PART -->
                <div class="col-md-6 col-sm-6 additional-shop-info">
                    <ul class="list-unstyled list-inline">
                        <li><i class="fa fa-phone"></i><span>+7 (347) 289-91-60</span></li>
                        <li><i class="fa fa-envelope-o"></i><span>company@progtb.ru</span></li>
                    </ul>
                </div>
                <!-- END TOP BAR LEFT PART -->
                <!-- BEGIN TOP BAR MENU -->
                <div class="col-md-6 col-sm-6 additional-nav">
                    <ul class="list-unstyled list-inline pull-right">
                        <?if ($this->auth->is_logged_in()):?>
                            <li>Привет, <a href="/admin"><?=$this->auth->user()->display_name?></a></li>
                            <li><a href="/index.php/logout">Выйти</a></li>
                        <?else:?>
                            <li><a href="/login">Войти</a></li>
                        <?endif?>
                        <li><a href="/blog">Блог</a></li>
                    </ul>
                </div>
                <!-- END TOP BAR MENU -->
            </div>
        </div>
    </div>
    <!-- END TOP BAR -->
    <div class="container">
        <div class="row">
            <div class="col-md-2 col-sm-2">
                <a class="scroll site-logo" href="/"><img src="/assets/frontend/onepage/img/logo/blue.png" alt="Логотип компании Прогрессивные Технологии Бизнеса"></a>
            </div>
            <a href="javascript:void(0);" class="mobi-toggler"><i class="fa fa-bars"></i></a>

            <!-- Navigation BEGIN -->
            <div class="col-md-10 pull-right">
                <ul class="header-navigation">
                    <li class="current"><a href="/#promo-block">Начало</a></li>
                    <li><a href="/#about">О нас</a></li>
                    <li><a href="/#services">Мы предлагаем и делаем</a></li>
                    <li><a href="/#team">Наша Команда</a></li>
                    <li><a href="/blog">Блог</a></li>
                    <li><a href="/#portfolio">Проекты</a></li>
                    <li><a href="/#feedback">Отзывы</a></li>
                    <li><a href="/#contact">Контакты</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- Header END -->