<?php

$num_columns	= 9;
$can_delete	= $this->auth->has_permission('Blog.Content.Delete');
$can_edit		= $this->auth->has_permission('Blog.Content.Edit');
$has_records	= isset($records) && is_array($records) && count($records);

if ($can_delete) {
    $num_columns++;
}
?>
<div class='admin-box'>
	<h3>
		<?php echo lang('blog_area_title'); ?>
	</h3>
	<?php echo form_open($this->uri->uri_string()); ?>
		<table class='table table-striped'>
			<thead>
				<tr>
					<?php if ($can_delete && $has_records) : ?>
					<th class='column-check'><input class='check-all' type='checkbox' /></th>
					<?php endif;?>
					
					<th><?php echo lang('blog_field_name'); ?></th>
					<th><?php echo lang('blog_field_category'); ?></th>
					<th><?php echo lang('blog_field_image'); ?></th>
					<th><?php echo lang('blog_field_tags'); ?></th>
					<th><?php echo lang('blog_column_deleted'); ?></th>
					<th><?php echo lang('blog_column_created'); ?></th>
					<th><?php echo lang('blog_column_vk_posting'); ?></th>
				</tr>
			</thead>
			<?php if ($has_records) : ?>
			<tfoot>
				<?php if ($can_delete) : ?>
				<tr>
					<td colspan='<?php echo $num_columns; ?>'>
						<?php echo lang('bf_with_selected'); ?>
						<input type='submit' name='delete' id='delete-me' class='btn btn-danger' value="<?php echo lang('bf_action_delete'); ?>" onclick="return confirm('<?php e(js_escape(lang('blog_delete_confirm'))); ?>')" />
					</td>
				</tr>
				<?php endif; ?>
			</tfoot>
			<?php endif; ?>
			<tbody>
				<?php
				if ($has_records) :
					foreach ($records as $record) :
				?>
				<tr>
					<?php if ($can_delete) : ?>
					<td class='column-check'><input type='checkbox' name='checked[]' value='<?php echo $record->id; ?>' /></td>
					<?php endif;?>
					
				<?php if ($can_edit) : ?>
					<td><?php echo anchor(SITE_AREA . '/content/blog/edit/' . $record->id, '<span class="icon-pencil"></span> ' .  $record->name); ?></td>
				<?php else : ?>
					<td><?php e($record->name); ?></td>
				<?php endif; ?>
					<td><?php e($record->category); ?></td>
					<td><?php e($record->image); ?></td>
					<td><?php e($record->tags); ?></td>
					<td><?php echo $record->deleted > 0 ? lang('blog_true') : lang('blog_false'); ?></td>
					<td><?php e($record->created_on); ?></td>
					<td><a href="javascript:void(0)" class="btn btn-primary" onclick="do_publication(this, '<?=$record->code?>')"><?php echo lang('blog_field_vk_posting'); ?></a></td>
				</tr>
				<?php
					endforeach;
				else:
				?>
				<tr>
					<td colspan='<?php echo $num_columns; ?>'><?php echo lang('blog_records_empty'); ?></td>
				</tr>
				<?php endif; ?>
			</tbody>
		</table>
	<?php
    echo form_close();
    
    echo $this->pagination->create_links();
    ?>
</div>