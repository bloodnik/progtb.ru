<?php
$validation_errors = validation_errors();
if ($validation_errors) :
?>
<div class='alert alert-block alert-error fade in'>
	<a class='close' data-dismiss='alert'>&times;</a>
	<h4 class='alert-heading'>
		<?php echo lang('blog_errors_message'); ?>
	</h4>
	<?php echo $validation_errors; ?>
</div>
<?php
endif;

$id = isset($blog->id) ? $blog->id : '';

?>
<div class='admin-box'>
	<h3>Новый пост</h3>
	<?php echo form_open_multipart($this->uri->uri_string(), 'class="form-horizontal"'); ?>
		<fieldset>
            

			<div class="control-group<?php echo form_error('name') ? ' error' : ''; ?>">
				<?php echo form_label(lang('blog_field_name'). lang('bf_form_label_required'), 'name', array('class' => 'control-label')); ?>
				<div class='controls'>
					<input id='name' type='text' required='required' name='name' maxlength='255' value="<?php echo set_value('name', isset($blog->name) ? $blog->name : ''); ?>" />
					<span class='help-inline'><?php echo form_error('name'); ?></span>
				</div>
			</div>
            <div class="control-group<?php echo form_error('code') ? ' error' : ''; ?>">
                <?php echo form_label(lang('blog_field_code'). lang('bf_form_label_required'), 'code', array('class' => 'control-label')); ?>
                <div class='controls'>
                    <input id='code' type='text' required='required' name='code' maxlength='255' value="<?php echo set_value('code', isset($blog->code) ? $blog->code : ''); ?>" />
                    <span class='help-inline'><?php echo form_error('code'); ?></span>
                </div>
            </div>
            <?=show_tinymce('text') ?>
			<div class="control-group<?php echo form_error('text') ? ' error' : ''; ?>">
				<?php echo form_label(lang('blog_field_text'), 'text', array('class' => 'control-label')); ?>
				<div class='controls'>
					<?php echo form_textarea(array('name' => 'text', 'id' => 'text', 'rows' => '5', 'cols' => '80', 'class' => 'tinyMCE', 'value' => set_value('text', isset($blog->text) ? $blog->text : ''))); ?>
					<span class='help-inline'><?php echo form_error('text'); ?></span>
				</div>
			</div>

			<?php // Change the values in this array to populate your dropdown as required
				echo form_dropdown(array('name' => 'category'), $bcategories, set_value('category', isset($blog->category) ? $blog->category : ''), lang('blog_field_category'));
			?>

			<div class="control-group<?php echo form_error('image') ? ' error' : ''; ?>">
				<?php echo form_label(lang('blog_field_image'), 'image', array('class' => 'control-label')); ?>
				<div class='controls'>
					<input id='image' type='file' name='image' maxlength='255' value="<?php echo set_value('image', isset($blog->image) ? $blog->image : ''); ?>" />
					<span class='help-inline'><?php echo form_error('image'); ?></span>
				</div>
			</div>

			<div class="control-group<?php echo form_error('tags') ? ' error' : ''; ?>">
				<?php echo form_label(lang('blog_field_tags'), 'tags', array('class' => 'control-label')); ?>
				<div class='controls'>
					<input id='tags' type='text' name='tags' maxlength='255' value="<?php echo set_value('tags', isset($blog->tags) ? $blog->tags : ''); ?>" />
					<span class='help-inline'><?php echo form_error('tags'); ?></span>
				</div>
			</div>

			<div class="control-group">
				<label for="role_id" class="control-label">Роль для отображения записей</label>
				<div class="controls">
					<select name="role_id" id="role_id" class="chzn-select">
						<?php
						if (isset($roles) && is_array($roles) && count($roles)) :
							foreach ($roles as $role) :
								// check if it should be the default
								$default_role = false;
								if ($role->role_id == 4) {
									$default_role = true;
								}
								?>
								<option value="<?=$role->role_id; ?>" <?=set_select('role_id', $role->role_id, $default_role); ?>>
									<?php e(ucfirst($role->role_name)); ?>
								</option>
							<?php
							endforeach;
						endif;
						?>
					</select>
				</div>
			</div>

            <div class="control-group<?php echo form_error('publication') ? ' error' : ''; ?>">
                <div class='controls'>
                    <label class='checkbox' for='publication'>
                        <input type='checkbox' id='publication' name='publication'  value='1' <?php echo set_checkbox('publication', 1, isset($blog->publication) && $blog->publication == 1); ?> />
                        <?=lang('blog_field_publication')?>
                    </label>
                    <span class='help-inline'><?php echo form_error('publication'); ?></span>
                </div>
            </div>

			<div class="control-group<?php echo form_error('show_count') ? ' error' : ''; ?>">
				<?php echo form_label(lang('blog_field_show_count'), 'show_count', array('class' => 'control-label')); ?>
				<div class='controls'>
					<input id='show_count' type='text' name='show_count' maxlength='4' value="<?php echo set_value('show_count', isset($blog->show_count) ? $blog->show_count : 0); ?>" />
					<span class='help-inline'><?php echo form_error('show_count'); ?></span>
				</div>
			</div>
        </fieldset>
		<fieldset class='form-actions'>
			<input type='submit' name='save' class='btn btn-primary' value="<?php echo lang('blog_action_create'); ?>" />
			<?php echo lang('bf_or'); ?>
			<?php echo anchor(SITE_AREA . '/content/blog', lang('blog_cancel'), 'class="btn btn-warning"'); ?>
			
		</fieldset>
    <?php echo form_close(); ?>
</div>