<?Template::block("header_nav_block")?>
    <div class="main">
    <div class="container">
    <ul class="breadcrumb">
        <li><a href="/">Главная</a></li>
        <li><a href="/blog">Блог</a></li>
        <li class="active"><?=$post->name?></li>
    </ul>
    <!-- BEGIN SIDEBAR & CONTENT -->
    <div class="row margin-bottom-40">
    <!-- BEGIN CONTENT -->
    <div class="col-md-12 col-sm-12">
    <?if (isset($error)):?>
        <h1><?=$error?></h1>
    <?else:?>
        <h1><?=$post->name?>
            <?if(!$post->publication):?>
                <span class="badge badge-primary">Черновик</span>
            <?endif?>
        </h1>
    <?endif;?>

    <div class="content-page">
    <div class="row">
    <!-- BEGIN LEFT SIDEBAR -->
    <div class="col-md-9 col-sm-9 blog-item">
        <?if (is_object($post)):?>
            <div class="blog-item-img">
                <div class="item">
                    <img src="<?=$post->image?>" alt="" style="max-width: 100%">
                </div>
            </div>
           <p><?=$post->text?></p>
            <ul class="blog-info">
                <li><i class="fa fa-user"></i><?=$this->auth->get_username_by_id($post->created_by)?></li>
                <li><i class="fa fa-calendar"></i> <?=$post->created_on?></li>
<!--                <li><i class="fa fa-comments"></i> 17</li>-->
                <li><i class="fa fa-tags"></i>
                    <?foreach ($detail_tags as $tag):?>
                        <a href="/blog?tag=<?=$tag?>"><?=$tag?></a>,
                    <?endforeach?>
                </li>

                <?if($this->auth->has_permission('Blog.Content.Edit', $this->auth->role_id())):?>
                    <li>
                        <a id="editPost" href="/admin/content/blog/edit/<?=$post->id?>" class="btn blue">Редактировать</a>
                    </li>
                <?endif;?>
            </ul>
            <script type="text/javascript">(function() {
                    if (window.pluso)if (typeof window.pluso.start == "function") return;
                    if (window.ifpluso==undefined) { window.ifpluso = 1;
                        var d = document, s = d.createElement('script'), g = 'getElementsByTagName';
                        s.type = 'text/javascript'; s.charset='UTF-8'; s.async = true;
                        s.src = ('https:' == window.location.protocol ? 'https' : 'http')  + '://share.pluso.ru/pluso-like.js';
                        var h=d[g]('body')[0];
                        h.appendChild(s);
                    }})();</script>
            <div class="pluso" data-background="none;" data-options="medium,square,line,horizontal,nocounter,sepcounter=1,theme=14" data-services="vkontakte,odnoklassniki,facebook,twitter,google,moimir"></div>
        <?endif;?>
    </div>
    <!-- END LEFT SIDEBAR -->


    <!-- BEGIN RIGHT SIDEBAR -->
    <div class="col-md-3 col-sm-3 blog-sidebar">
        <?Template::block('category_block'); //Подключаем блок категорий?>
        <div class="fixed_sidebar">
            <?Template::block('recent_block'); //Подключаем блок последних постов?>
            <?Template::block('tags_block'); //Подключаем блок последних постов?>
        </div>
    </div>
    <!-- END RIGHT SIDEBAR -->
    </div>
    </div>
    </div>
    <!-- END CONTENT -->
    </div>
    <!-- END SIDEBAR & CONTENT -->
    </div>
    </div>
<?Template::block("footer_block")?>


<!-- Load javascripts at bottom, this will reduce page load time -->
<!-- BEGIN CORE PLUGINS (REQUIRED FOR ALL PAGES) -->
<!--[if lt IE 9]>
<script src="/assets/global/plugins/respond.min.js"></script>
<![endif]-->
<script src="/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="/assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<script src="/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="/assets/frontend/layout/scripts/back-to-top.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->

<!-- BEGIN PAGE LEVEL JAVASCRIPTS (REQUIRED ONLY FOR CURRENT PAGE) -->
<script src="/assets/global/plugins/fancybox/source/jquery.fancybox.pack.js" type="text/javascript"></script><!-- pop up -->

<script src="/assets/frontend/layout/scripts/layout.js" type="text/javascript"></script>

<?echo Assets::js();?>

<!-- END PAGE LEVEL JAVASCRIPTS -->
</body>
<!-- END BODY -->
