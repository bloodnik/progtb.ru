<!-- BEGIN PRE-FOOTER -->
<div class="pre-footer" id="contact">
    <div class="container">
        <div class="row">
            <!-- BEGIN BOTTOM ABOUT BLOCK -->
            <div class="col-md-4 col-sm-6 pre-footer-col">
                <h2>Регионы</h2>
                <p>Наши клиенты ведут свой бизнес в различных городах России. География наших внедрений широка: Центральный федеральный округ, Южный федеральный округ, Сибирский федеральный округ, Уральский федеральный округ, Приволжский федеральный округ.</p>
                <p>Мы не ограничиваемся работой через интернет, готовы приехать и работать на вашей территории.</p>
            </div>
            <!-- END BOTTOM ABOUT BLOCK -->

            <!-- BEGIN TIMEWORK BLOCK -->
            <div class="col-md-4 col-sm-6 pre-footer-col">
                <h2>График работы</h2>
                <b>Понедельник: </b>8:30 - 18:00<br>
                <b>Вторник: </b>8:30 - 18:00<br>
                <b>Среда: </b>8:30 - 18:00<br>
                <b>Четврерг: </b>8:30 - 18:00<br>
                <b>Пятница: </b>8:30 - 16:00<br>
                <b>Суббота, Воскресенье: </b><span>Выходной</span><br><br>
                <p>Указано местное время.<br>Для Москвы +02:00<br>GMT +06:00</p>
            </div>
            <!-- END TIMEWORK BLOCK -->

            <div class="col-md-4 col-sm-6 pre-footer-col">
                <!-- BEGIN BOTTOM CONTACTS -->
                <h2>Наши Контакты</h2>
                <address class="margin-bottom-20">
                    450006, Республика Башкортостан<br>
                    г. Уфа, ул. Ленина, 99, офис 12/1<br>
                    Телефон: +7 (347) 289-91-60<br>
                    Факс: +7 (347) 289-91-91<br>
                    Email: <a href="mailto:company@progtb.ru">company@progtb.ru</a><br>
                    Skype: <a href="skype:progtb">progtb</a>
                </address>
                <!-- END BOTTOM CONTACTS -->

                <div class="pre-footer-subscribe-box">
                    <h2>Новости</h2>
                    <form action="javascript:void(0);">
                        <div class="input-group">
                            <input type="text" placeholder="адрес эл.почты" class="form-control">
                <span class="input-group-btn">
                  <button class="btn btn-primary" type="submit">Подписаться</button>
                </span>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END PRE-FOOTER -->

<!-- BEGIN FOOTER -->
<div class="footer">
    <div class="container">
        <div class="row">
            <!-- BEGIN COPYRIGHT -->
            <div class="col-md-6 col-sm-6">
                <div class="copyright"><?php echo date('Y'); ?> © Прогрессивные Технологии Бизнеса.</div>
            </div>
            <!-- END COPYRIGHT -->
            <!-- BEGIN SOCIAL ICONS
            <div class="col-md-6 col-sm-6 pull-right">
              <ul class="social-icons">
                <li><a class="rss" data-original-title="rss" href="javascript:void(0);"></a></li>
                <li><a class="twitter" data-original-title="twitter" href="javascript:void(0);"></a></li>
                <li><a class="googleplus" data-original-title="googleplus" href="javascript:void(0);"></a></li>
                <li><a class="linkedin" data-original-title="linkedin" href="javascript:void(0);"></a></li>
                <li><a class="youtube" data-original-title="youtube" href="javascript:void(0);"></a></li>
                <li><a class="skype" data-original-title="skype" href="javascript:void(0);"></a></li>
              </ul>
            </div>
            <!-- END SOCIAL ICONS -->
        </div>
    </div>
</div>
<!-- END FOOTER -->