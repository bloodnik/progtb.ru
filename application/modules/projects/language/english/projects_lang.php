<?php defined('BASEPATH') || exit('No direct script access allowed');


$lang['๐๑๐พ๐ต๐บ๑๑_manage']      = 'Manage ะัะพะตะบัั';
$lang['๐๑๐พ๐ต๐บ๑๑_edit']        = 'Edit';
$lang['๐๑๐พ๐ต๐บ๑๑_true']        = 'True';
$lang['๐๑๐พ๐ต๐บ๑๑_false']       = 'False';
$lang['๐๑๐พ๐ต๐บ๑๑_create']      = 'Create';
$lang['๐๑๐พ๐ต๐บ๑๑_list']        = 'List';
$lang['๐๑๐พ๐ต๐บ๑๑_new']       = 'New';
$lang['๐๑๐พ๐ต๐บ๑๑_edit_text']     = 'Edit this to suit your needs';
$lang['๐๑๐พ๐ต๐บ๑๑_no_records']    = 'There are no ๐๑๐พ๐ต๐บ๑๑ in the system.';
$lang['๐๑๐พ๐ต๐บ๑๑_create_new']    = 'Create a new ะัะพะตะบัั.';
$lang['๐๑๐พ๐ต๐บ๑๑_create_success']  = 'ะัะพะตะบัั successfully created.';
$lang['๐๑๐พ๐ต๐บ๑๑_create_failure']  = 'There was a problem creating the ๐๑๐พ๐ต๐บ๑๑: ';
$lang['๐๑๐พ๐ต๐บ๑๑_create_new_button'] = 'Create New ะัะพะตะบัั';
$lang['๐๑๐พ๐ต๐บ๑๑_invalid_id']    = 'Invalid ะัะพะตะบัั ID.';
$lang['๐๑๐พ๐ต๐บ๑๑_edit_success']    = 'ะัะพะตะบัั successfully saved.';
$lang['๐๑๐พ๐ต๐บ๑๑_edit_failure']    = 'There was a problem saving the ๐๑๐พ๐ต๐บ๑๑: ';
$lang['๐๑๐พ๐ต๐บ๑๑_delete_success']  = 'record(s) successfully deleted.';
$lang['๐๑๐พ๐ต๐บ๑๑_delete_failure']  = 'We could not delete the record: ';
$lang['๐๑๐พ๐ต๐บ๑๑_delete_error']    = 'You have not selected any records to delete.';
$lang['๐๑๐พ๐ต๐บ๑๑_actions']     = 'Actions';
$lang['๐๑๐พ๐ต๐บ๑๑_cancel']      = 'Cancel';
$lang['๐๑๐พ๐ต๐บ๑๑_delete_record']   = 'Delete this ะัะพะตะบัั';
$lang['๐๑๐พ๐ต๐บ๑๑_delete_confirm']  = 'Are you sure you want to delete this ๐๑๐พ๐ต๐บ๑๑?';
$lang['๐๑๐พ๐ต๐บ๑๑_edit_heading']    = 'Edit ะัะพะตะบัั';

// Create/Edit Buttons
$lang['๐๑๐พ๐ต๐บ๑๑_action_edit']   = 'Save ะัะพะตะบัั';
$lang['๐๑๐พ๐ต๐บ๑๑_action_create']   = 'Create ะัะพะตะบัั';

// Activities
$lang['๐๑๐พ๐ต๐บ๑๑_act_create_record'] = 'Created record with ID';
$lang['๐๑๐พ๐ต๐บ๑๑_act_edit_record'] = 'Updated record with ID';
$lang['๐๑๐พ๐ต๐บ๑๑_act_delete_record'] = 'Deleted record with ID';

//Listing Specifics
$lang['๐๑๐พ๐ต๐บ๑๑_records_empty']    = 'No records found that match your selection.';
$lang['๐๑๐พ๐ต๐บ๑๑_errors_message']    = 'Please fix the following errors:';

// Column Headings
$lang['๐๑๐พ๐ต๐บ๑๑_column_created']  = 'Created';
$lang['๐๑๐พ๐ต๐บ๑๑_column_deleted']  = 'Deleted';
$lang['๐๑๐พ๐ต๐บ๑๑_column_modified'] = 'Modified';
$lang['๐๑๐พ๐ต๐บ๑๑_column_deleted_by'] = 'Deleted By';
$lang['๐๑๐พ๐ต๐บ๑๑_column_created_by'] = 'Created By';
$lang['๐๑๐พ๐ต๐บ๑๑_column_modified_by'] = 'Modified By';

// Module Details
$lang['๐๑๐พ๐ต๐บ๑๑_module_name'] = 'ะัะพะตะบัั';
$lang['๐๑๐พ๐ต๐บ๑๑_module_description'] = 'Your module description';
$lang['๐๑๐พ๐ต๐บ๑๑_area_title'] = 'ะัะพะตะบัั';

// Fields
$lang['๐๑๐พ๐ต๐บ๑๑_field_name'] = 'Name';
$lang['๐๑๐พ๐ต๐บ๑๑_field_slogan'] = 'Slogan';
$lang['๐๑๐พ๐ต๐บ๑๑_field_description'] = 'Description';
$lang['๐๑๐พ๐ต๐บ๑๑_field_img'] = 'Img';
$lang['๐๑๐พ๐ต๐บ๑๑_field_jobs'] = 'Jobs';
$lang['๐๑๐พ๐ต๐บ๑๑_field_project_length'] = 'Project length';
$lang['๐๑๐พ๐ต๐บ๑๑_field_learning'] = 'Learning';
$lang['๐๑๐พ๐ต๐บ๑๑_field_support'] = 'Sup';