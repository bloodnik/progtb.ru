<?php defined('BASEPATH') || exit('No direct script access allowed');

$lang['�������_manage']      = 'Gerenciar Проекты';
$lang['�������_edit']        = 'Editar';
$lang['�������_true']        = 'Verdadeiro';
$lang['�������_false']       = 'Falso';
$lang['�������_create']      = 'Criar';
$lang['�������_list']        = 'Listar';
$lang['�������_new']       = 'Novo';
$lang['�������_edit_text']     = 'Edite isto conforme sua necessidade';
$lang['�������_no_records']    = 'Não há ������� no sistema.';
$lang['�������_create_new']    = 'Criar novo(a) Проекты.';
$lang['�������_create_success']  = 'Проекты Criado(a) com sucesso.';
$lang['�������_create_failure']  = 'Ocorreu um problema criando o(a) �������: ';
$lang['�������_create_new_button'] = 'Criar novo(a) Проекты';
$lang['�������_invalid_id']    = 'ID de Проекты inválida.';
$lang['�������_edit_success']    = 'Проекты salvo(a) com sucesso.';
$lang['�������_edit_failure']    = 'Ocorreu um problema salvando o(a) �������: ';
$lang['�������_delete_success']  = 'Registro(s) excluído(s) com sucesso.';
$lang['�������_delete_failure']  = 'Não foi possível excluir o registro: ';
$lang['�������_delete_error']    = 'Voc6e não selecionou nenhum registro para excluir.';
$lang['�������_actions']     = 'Ações';
$lang['�������_cancel']      = 'Cancelar';
$lang['�������_delete_record']   = 'Excluir este(a) Проекты';
$lang['�������_delete_confirm']  = 'Você tem certeza que deseja excluir este(a) �������?';
$lang['�������_edit_heading']    = 'Editar Проекты';

// Create/Edit Buttons
$lang['�������_action_edit']   = 'Salvar Проекты';
$lang['�������_action_create']   = 'Criar Проекты';

// Activities
$lang['�������_act_create_record'] = 'Criado registro com ID';
$lang['�������_act_edit_record'] = 'Atualizado registro com ID';
$lang['�������_act_delete_record'] = 'Excluído registro com ID';

//Listing Specifics
$lang['�������_records_empty']    = 'Nenhum registro encontrado.';
$lang['�������_errors_message']    = 'Por favor corrija os erros a seguir:';

// Column Headings
$lang['�������_column_created']  = 'Criado';
$lang['�������_column_deleted']  = 'Excluído';
$lang['�������_column_modified'] = 'Atualizado';

// Module Details
$lang['�������_module_name'] = 'Проекты';
$lang['�������_module_description'] = 'Your module description';
$lang['�������_area_title'] = 'Проекты';

// Fields
$lang['�������_field_name'] = 'Name';
$lang['�������_field_slogan'] = 'Slogan';
$lang['�������_field_description'] = 'Description';
$lang['�������_field_img'] = 'Img';
$lang['�������_field_jobs'] = 'Jobs';
$lang['�������_field_project_length'] = 'Project length';
$lang['�������_field_learning'] = 'Learning';
$lang['�������_field_support'] = 'Sup';
