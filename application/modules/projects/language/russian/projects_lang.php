<?php defined('BASEPATH') || exit('No direct script access allowed');


$lang['projects_manage']      = 'Управление проектами';
$lang['projects_edit']        = 'Редактировать';
$lang['projects_true']        = 'Верно';
$lang['projects_false']       = 'Не врено';
$lang['projects_create']      = 'Создать';
$lang['projects_list']        = 'Список проектов';
$lang['projects_new']       = 'Новый проект';
$lang['projects_edit_text']     = 'Отредактируте проект если вам это необходимо';
$lang['projects_no_records']    = 'Нет ни одной записи.';
$lang['projects_create_new']    = 'Добавить новый проетк.';
$lang['projects_create_success']  = 'Проект успешно добавлен.';
$lang['projects_create_failure']  = 'Есть некоторые проблемы при создании: ';
$lang['projects_create_new_button'] = 'Создать новые проекты';
$lang['projects_invalid_id']    = 'Неверный ID проекта.';
$lang['projects_edit_success']    = 'Проект успешно сохранен.';
$lang['projects_edit_failure']    = 'Есть проблемы при сохранении: ';
$lang['projects_delete_success']  = 'Запись(и) успешно удалена.';
$lang['projects_delete_failure']  = 'Не возможно удалить проект: ';
$lang['projects_delete_error']    = 'Не выбран ни один проект для удаления.';
$lang['projects_actions']     = 'Действия';
$lang['projects_cancel']      = 'Отмена';
$lang['projects_delete_record']   = 'Удалить этот проект';
$lang['projects_delete_confirm']  = 'Вы действительно хотите удалить этот проект?';
$lang['projects_edit_heading']    = 'Редактирование проекта';

// Create/Edit Buttons
$lang['projects_action_edit']   = 'Сохранить проект';
$lang['projects_action_create']   = 'Создать проект';

// Activities
$lang['projects_act_create_record'] = 'Создать запись с ID';
$lang['projects_act_edit_record'] = 'Обновить запись с ID';
$lang['projects_act_delete_record'] = 'Удалить запись с ID';

//Listing Specifics
$lang['projects_records_empty']    = 'Не найдены удовлетворяющие вас записи.';
$lang['projects_errors_message']    = 'Пожалуйста, исправте следующие проблемы:';

// Column Headings
$lang['projects_column_created']  = 'Создано';
$lang['projects_column_deleted']  = 'Удалено';
$lang['projects_column_modified'] = 'Обновлено';
$lang['projects_column_deleted_by'] = 'Удалено ';
$lang['projects_column_created_by'] = 'Создано';
$lang['projects_column_modified_by'] = 'Обновлено';

// Module Details
$lang['projects_module_name'] = 'Проекты';
$lang['projects_module_description'] = 'Наши проекты';
$lang['projects_area_title'] = 'Проекты';

// Fields
$lang['projects_field_name'] = 'Наименование';
$lang['projects_field_slogan'] = 'Слоган';
$lang['projects_field_description'] = 'Описание';
$lang['projects_field_img'] = 'Изображение';
$lang['projects_field_jobs'] = 'Количество мест';
$lang['projects_field_project_length'] = 'Срок проекта';
$lang['projects_field_learning'] = 'Обучение';
$lang['projects_field_support'] = 'Поддержка';