<?php defined('BASEPATH') || exit('No direct script access allowed');

$lang['�������_manage']      = 'Gestisci Проекты';
$lang['�������_edit']        = 'Modifica';
$lang['�������_true']        = 'Vero';
$lang['�������_false']       = 'Falso';
$lang['�������_create']      = 'Crea';
$lang['�������_list']        = 'Elenca';
$lang['�������_new']       = 'Nuovo';
$lang['�������_edit_text']     = 'Modifica questo secondo le tue necessità';
$lang['�������_no_records']    = 'Non ci sono ������� nel sistema.';
$lang['�������_create_new']    = 'Crea un nuovo Проекты.';
$lang['�������_create_success']  = 'Проекты creato con successo.';
$lang['�������_create_failure']  = 'C\'è stato un problema nella creazione di �������: ';
$lang['�������_create_new_button'] = 'Crea nuovo Проекты';
$lang['�������_invalid_id']    = 'ID Проекты non valido.';
$lang['�������_edit_success']    = 'Проекты creato con successo.';
$lang['�������_edit_failure']    = 'C\'è stato un errore nel salvataggio di �������: ';
$lang['�������_delete_success']  = 'record(s) creati con successo.';
$lang['�������_delete_failure']  = 'Non possiamo eliminare il record: ';
$lang['�������_delete_error']    = 'Non hai selezionato alcun record da eliminare.';
$lang['�������_actions']     = 'Azioni';
$lang['�������_cancel']      = 'Cancella';
$lang['�������_delete_record']   = 'Elimina questo Проекты';
$lang['�������_delete_confirm']  = 'Sei sicuro di voler eliminare questo �������?';
$lang['�������_edit_heading']    = 'Modifica Проекты';

// Create/Edit Buttons
$lang['�������_action_edit']   = 'Salva Проекты';
$lang['�������_action_create']   = 'Crea Проекты';

// Activities
$lang['�������_act_create_record'] = 'Creato il record con ID';
$lang['�������_act_edit_record'] = 'Aggiornato il record con ID';
$lang['�������_act_delete_record'] = 'Eliminato il record con ID';

// Listing Specifics
$lang['�������_records_empty']    = 'Nessun record trovato che corrisponda alla tua selezione.';
$lang['�������_errors_message']    = 'Per favore risolvi i seguenti errori:';

// Column Headings
$lang['�������_column_created']  = 'Creato';
$lang['�������_column_deleted']  = 'Eliminato';
$lang['�������_column_modified'] = 'Modificato';

// Module Details
$lang['�������_module_name'] = 'Проекты';
$lang['�������_module_description'] = 'Your module description';
$lang['�������_area_title'] = 'Проекты';

// Fields
$lang['�������_field_name'] = 'Name';
$lang['�������_field_slogan'] = 'Slogan';
$lang['�������_field_description'] = 'Description';
$lang['�������_field_img'] = 'Img';
$lang['�������_field_jobs'] = 'Jobs';
$lang['�������_field_project_length'] = 'Project length';
$lang['�������_field_learning'] = 'Learning';
$lang['�������_field_support'] = 'Sup';
