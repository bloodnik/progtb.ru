<?php defined('BASEPATH') || exit('No direct script access allowed');

$lang['�������_manage']      = 'Gestionar Проекты';
$lang['�������_edit']        = 'Editar';
$lang['�������_true']        = 'Verdadero';
$lang['�������_false']       = 'Falso';
$lang['�������_create']      = 'Crear';
$lang['�������_list']        = 'Listar';
$lang['�������_new']       = 'Nuevo';
$lang['�������_edit_text']     = 'Editar esto para satisfacer sus necesidades';
$lang['�������_no_records']    = 'Hay ninguna ������� en la sistema.';
$lang['�������_create_new']    = 'Crear nuevo(a) Проекты.';
$lang['�������_create_success']  = 'Проекты creado(a) con éxito.';
$lang['�������_create_failure']  = 'Hubo un problema al crear el(la) �������: ';
$lang['�������_create_new_button'] = 'Crear nuevo(a) Проекты';
$lang['�������_invalid_id']    = 'ID de Проекты inválido(a).';
$lang['�������_edit_success']    = 'Проекты guardado correctamente.';
$lang['�������_edit_failure']    = 'Hubo un problema guardando el(la) �������: ';
$lang['�������_delete_success']  = 'Registro(s) eliminado con éxito.';
$lang['�������_delete_failure']  = 'No hemos podido eliminar el registro: ';
$lang['�������_delete_error']    = 'No ha seleccionado ning&#250;n registro que desea eliminar.';
$lang['�������_actions']     = 'Açciones';
$lang['�������_cancel']      = 'Cancelar';
$lang['�������_delete_record']   = 'Eliminar este(a) Проекты';
$lang['�������_delete_confirm']  = '¿Esta seguro de que desea eliminar este(a) �������?';
$lang['�������_edit_heading']    = 'Editar Проекты';

// Create/Edit Buttons
$lang['�������_action_edit']   = 'Guardar Проекты';
$lang['�������_action_create']   = 'Crear Проекты';

// Activities
$lang['�������_act_create_record'] = 'Creado registro con ID';
$lang['�������_act_edit_record'] = 'Actualizado registro con ID';
$lang['�������_act_delete_record'] = 'Eliminado registro con ID';

//Listing Specifics
$lang['�������_records_empty']    = 'No hay registros encontrados para su selección.';
$lang['�������_errors_message']    = 'Por favor corrija los siguientes errores:';

// Column Headings
$lang['�������_column_created']  = 'Creado';
$lang['�������_column_deleted']  = 'Elíminado';
$lang['�������_column_modified'] = 'Modificado';

// Module Details
$lang['�������_module_name'] = 'Проекты';
$lang['�������_module_description'] = 'Your module description';
$lang['�������_area_title'] = 'Проекты';

// Fields
$lang['�������_field_name'] = 'Name';
$lang['�������_field_slogan'] = 'Slogan';
$lang['�������_field_description'] = 'Description';
$lang['�������_field_img'] = 'Img';
$lang['�������_field_jobs'] = 'Jobs';
$lang['�������_field_project_length'] = 'Project length';
$lang['�������_field_learning'] = 'Learning';
$lang['�������_field_support'] = 'Sup';
