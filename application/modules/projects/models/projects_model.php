<?php defined('BASEPATH') || exit('No direct script access allowed');

class projects_model extends BF_Model
{
    protected $table_name	= 'projects';
	protected $key			= 'id';
	protected $date_format	= 'datetime';

	protected $log_user 	= false;
	protected $set_created	= false;
	protected $set_modified = false;
	protected $soft_deletes	= false;


	// Customize the operations of the model without recreating the insert,
    // update, etc. methods by adding the method names to act as callbacks here.
	protected $before_insert 	= array();
	protected $after_insert 	= array();
	protected $before_update 	= array();
	protected $after_update 	= array();
	protected $before_find 	    = array();
	protected $after_find 		= array();
	protected $before_delete 	= array();
	protected $after_delete 	= array();

	// For performance reasons, you may require your model to NOT return the id
	// of the last inserted row as it is a bit of a slow method. This is
    // primarily helpful when running big loops over data.
	protected $return_insert_id = true;

	// The default type for returned row data.
	protected $return_type = 'object';

	// Items that are always removed from data prior to inserts or updates.
	protected $protected_attributes = array();

	// You may need to move certain rules (like required) into the
	// $insert_validation_rules array and out of the standard validation array.
	// That way it is only required during inserts, not updates which may only
	// be updating a portion of the data.
	protected $validation_rules 		= array(
		array(
			'field' => 'name',
			'label' => 'lang:projects_field_name',
			'rules' => 'required|trim|max_length[255]',
		),
		array(
			'field' => 'slogan',
			'label' => 'lang:projects_field_slogan',
			'rules' => 'trim|max_length[255]',
		),
		array(
			'field' => 'description',
			'label' => 'lang:projects_field_description',
			'rules' => 'required|trim|max_length[1000]',
		),
		array(
			'field' => 'img',
			'label' => 'lang:projects_field_img',
			'rules' => '',
		),
		array(
			'field' => 'jobs',
			'label' => 'lang:projects_field_jobs',
			'rules' => 'trim|max_length[3]',
		),
		array(
			'field' => 'project_length',
			'label' => 'lang:projects_field_project_length',
			'rules' => 'trim|max_length[3]',
		),
		array(
			'field' => 'learning',
			'label' => 'lang:projects_field_learning',
			'rules' => '',
		),
		array(
			'field' => 'support',
			'label' => 'lang:projects_field_support',
			'rules' => '',
		),
	);
	protected $insert_validation_rules  = array();
	protected $skip_validation 			= false;

    /**
     * Constructor
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
}