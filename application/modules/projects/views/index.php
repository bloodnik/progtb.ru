<?php

$hiddenFields = array('id',);
?>
<h1 class='page-header'>
    <?php echo lang('projects_area_title'); ?>
</h1>
<?php if (isset($records) && is_array($records) && count($records)) : ?>
<table class='table table-striped table-bordered'>
    <thead>
        <tr>
            
            <th>Наименование</th>
            <th>Слоган</th>
            <th>Описание</th>
            <th>Изображение</th>
            <th>Количество мест</th>
            <th>Срок проекта</th>
            <th>Обучение</th>
            <th>Поддержка</th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($records as $record) :
        ?>
        <tr>
            <?php
            foreach($record as $field => $value) :
                if ( ! in_array($field, $hiddenFields)) :
            ?>
            <td>
                <?php
                if ($field == 'deleted') {
                    e(($value > 0) ? lang('projects_true') : lang('projects_false'));
                } else {
                    e($value);
                }
                ?>
            </td>
            <?php
                endif;
            endforeach;
            ?>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
    <?php echo Template::block('new_block'); ?>
<?php

    echo $this->pagination->create_links();
endif; ?>