<?php

$num_columns	= 8;
$can_delete	= true;
$can_edit		= true;
$has_records	= isset($records) && is_array($records) && count($records);

if ($can_delete) {
    $num_columns++;
}
?>
<div class='admin-box'>
	<h3>
		<?php echo lang('projects_area_title');?>
	</h3>
	<?php echo form_open($this->uri->uri_string()); ?>
		<table class='table table-striped'>
			<thead>
				<tr>
					<?php if ($can_delete && $has_records) : ?>
					<th class='column-check'><input class='check-all' type='checkbox' /></th>
					<?php endif;?>
					
					<th><?php echo lang('projects_field_name'); ?></th>
					<th><?php echo lang('projects_field_slogan'); ?></th>
					<th><?php echo lang('projects_field_description'); ?></th>
					<th><?php echo lang('projects_field_img'); ?></th>
					<th><?php echo lang('projects_field_jobs'); ?></th>
					<th><?php echo lang('projects_field_project_length'); ?></th>
					<th><?php echo lang('projects_field_learning'); ?></th>
					<th><?php echo lang('projects_field_support'); ?></th>
				</tr>
			</thead>
			<?php if ($has_records) : ?>
			<tfoot>
				<?php if ($can_delete) : ?>
				<tr>
					<td colspan='<?php echo $num_columns; ?>'>
						<?php echo lang('bf_with_selected'); ?>
						<input type='submit' name='delete' id='delete-me' class='btn btn-danger' value="<?php echo lang('bf_action_delete'); ?>" onclick="return confirm('<?php e(js_escape(lang('projects_delete_confirm'))); ?>')" />
					</td>
				</tr>
				<?php endif; ?>
			</tfoot>
			<?php endif; ?>
			<tbody>
				<?php
				if ($has_records) :
					foreach ($records as $record) :
				?>
				<tr>
					<?php if ($can_delete) : ?>
					<td class='column-check'><input type='checkbox' name='checked[]' value='<?php echo $record->id; ?>' /></td>
					<?php endif;?>
				<?php if ($can_edit) : ?>
					<td><?php echo anchor(SITE_AREA . '/content/projects/edit/' . $record->id, '<span class="icon-pencil"></span> ' .  $record->name); ?></td>
				<?php else : ?>
					<td><?php e($record->name); ?></td>
				<?php endif; ?>
					<td><?php e($record->slogan); ?></td>
					<td><?php e(word_limiter($record->description, 40)); ?></td>
					<td><?php e($record->img); ?></td>
					<td><?php e($record->jobs); ?></td>
					<td><?php e($record->project_length); ?></td>
					<td><?php e($record->learning); ?></td>
					<td><?php e($record->support); ?></td>
				</tr>
				<?php
					endforeach;
				else:
				?>
				<tr>
					<td colspan='<?php echo $num_columns; ?>'><?php echo lang('projects_records_empty'); ?></td>
				</tr>
				<?php endif; ?>
			</tbody>
		</table>
	<?php
    echo form_close();
    
    echo $this->pagination->create_links();
    ?>
</div>