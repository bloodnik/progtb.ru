<?php

$validation_errors = validation_errors();

if ($validation_errors) :
?>
<div class='alert alert-block alert-error fade in'>
	<a class='close' data-dismiss='alert'>&times;</a>
	<h4 class='alert-heading'>
		<?php echo lang('projects_errors_message'); ?>
	</h4>
	<?php echo $validation_errors; ?>
</div>
<?php
endif;

$id = isset($projects->id) ? $projects->id : '';

?>
<div class='admin-box'>
    <h3><?php echo lang('projects_new'); ?></h3>
	<?php echo form_open_multipart($this->uri->uri_string(), 'class="form-horizontal"'); ?>
		<fieldset>
            

			<div class="control-group<?php echo form_error('name') ? ' error' : ''; ?>">
				<?php echo form_label('Наименование'. lang('bf_form_label_required'), 'name', array('class' => 'control-label')); ?>
				<div class='controls'>
					<input id='name' type='text' required='required' name='name' maxlength='255' value="<?php echo set_value('name', isset($projects->name) ? $projects->name : ''); ?>" />
					<span class='help-inline'><?php echo form_error('name'); ?></span>
				</div>
			</div>

			<div class="control-group<?php echo form_error('slogan') ? ' error' : ''; ?>">
				<?php echo form_label('Слоган', 'slogan', array('class' => 'control-label')); ?>
				<div class='controls'>
					<input id='slogan' type='text' name='slogan' maxlength='255' value="<?php echo set_value('slogan', isset($projects->slogan) ? $projects->slogan : ''); ?>" />
					<span class='help-inline'><?php echo form_error('slogan'); ?></span>
				</div>
			</div>

			<div class="control-group<?php echo form_error('description') ? ' error' : ''; ?>">
				<?php echo form_label('Описание'. lang('bf_form_label_required'), 'description', array('class' => 'control-label')); ?>
				<div class='controls'>
					<?php echo form_textarea(array('name' => 'description', 'id' => 'description', 'rows' => '5', 'cols' => '80', 'value' => set_value('description', isset($projects->description) ? $projects->description : ''), 'required' => 'required')); ?>
					<span class='help-inline'><?php echo form_error('description'); ?></span>
				</div>
			</div>

			<div class="control-group<?php echo form_error('img') ? ' error' : ''; ?>">
				<?php echo form_label('Изображение (размер изображения 900х750)'. lang('bf_form_label_required'), 'img', array('class' => 'control-label')); ?>
				<div class='controls'>
					<input id='image' type='file' name='image' maxlength='255' value="<?php echo set_value('img', isset($projects->img) ? $projects->img : ''); ?>" />
					<span class='help-inline'><?php echo form_error('img'); ?></span>
				</div>
			</div>

			<div class="control-group<?php echo form_error('jobs') ? ' error' : ''; ?>">
				<?php echo form_label('Количество мест', 'jobs', array('class' => 'control-label')); ?>
				<div class='controls'>
					<input id='jobs' type='text' name='jobs' maxlength='3' value="<?php echo set_value('jobs', isset($projects->jobs) ? $projects->jobs : '0'); ?>" />
					<span class='help-inline'><?php echo form_error('jobs'); ?></span>
				</div>
			</div>

			<div class="control-group<?php echo form_error('project_length') ? ' error' : ''; ?>">
				<?php echo form_label('Срок проекта', 'project_length', array('class' => 'control-label')); ?>
				<div class='controls'>
					<input id='project_length' type='text' name='project_length' maxlength='3' value="<?php echo set_value('project_length', isset($projects->project_length) ? $projects->project_length : '0'); ?>" />
					<span class='help-inline'><?php echo form_error('project_length'); ?></span>
				</div>
			</div>

			<div class="control-group<?php echo form_error('learning') ? ' error' : ''; ?>">
				<div class='controls'>
					<label class='checkbox' for='learning'>
						<input type='checkbox' id='learning' name='learning'  value='1' <?php echo set_checkbox('learning', 1, isset($projects->learning) && $projects->learning == 1); ?> />
                        'Обучение'
					</label>
                    <span class='help-inline'><?php echo form_error('learning'); ?></span>
				</div>
			</div>

			<div class="control-group<?php echo form_error('support') ? ' error' : ''; ?>">
				<div class='controls'>
					<label class='checkbox' for='support'>
						<input type='checkbox' id='support' name='support'  value='1' <?php echo set_checkbox('support', 1, isset($projects->support) && $projects->support == 1); ?> />
                        'Поддержка'
					</label>
                    <span class='help-inline'><?php echo form_error('support'); ?></span>
				</div>
			</div>
        </fieldset>
		<fieldset class='form-actions'>
			<input type='submit' name='save' class='btn btn-primary' value="<?php echo lang('projects_action_create'); ?>" />
			<?php echo lang('bf_or'); ?>
			<?php echo anchor(SITE_AREA . '/content/projects', lang('projects_cancel'), 'class="btn btn-warning"'); ?>
			
		</fieldset>
    <?php echo form_close(); ?>
</div>