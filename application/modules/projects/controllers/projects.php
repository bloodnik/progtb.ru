<?php defined('BASEPATH') || exit('No direct script access allowed');

/**
 * projects controller
 */
class projects extends Front_Controller
{
    protected $permissionCreate = 'projects.projects.Create';
    protected $permissionDelete = 'projects.projects.Delete';
    protected $permissionEdit   = 'projects.projects.Edit';
    protected $permissionView   = 'projects.projects.View';

    /**
	 * Constructor
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
		
		$this->load->model('projects/projects_model');
        $this->lang->load('projects');
		
			Assets::add_js(Template::theme_url('js/editors/ckeditor/ckeditor.js'));
        

		Assets::add_module_js('projects', 'projects.js');
        Template::set_block('new_block', '_new_block');
	}

	/**
	 * Display a list of projects data.
	 *
	 * @return void
	 */
	public function index($offset = 0)
	{
        
        $pagerUriSegment = 3;
        $pagerBaseUrl = site_url('projects/index') . '/';
        
        $limit  = $this->settings_lib->item('site.list_limit') ?: 15;

        $this->load->library('pagination');
        $pager['base_url']    = $pagerBaseUrl;
        $pager['total_rows']  = $this->projects_model->count_all();
        $pager['per_page']    = $limit;
        $pager['uri_segment'] = $pagerUriSegment;

        $this->pagination->initialize($pager);
        $this->projects_model->limit($limit, $offset);
        
		$records = $this->projects_model->find_all();

		Template::set('records', $records);
        

		Template::render();
	}
    
}