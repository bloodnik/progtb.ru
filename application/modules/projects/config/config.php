<?php defined('BASEPATH') || exit('No direct script access allowed');

$config['module_config'] = array(
	'description'	=> 'Наши проекты',
	'name'		    => 'Проекты',
     /*
      * Replace the 'name' entry above with this entry and create the entry in
      * the application_lang file for localization/translation support in the
      * menu
     'name'          => 'lang:bf_menu_projects',
      */
	'version'		=> '0.0.1',
	'author'		=> 'root',
);