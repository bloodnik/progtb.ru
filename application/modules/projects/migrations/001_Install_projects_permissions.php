<?php defined('BASEPATH') || exit('No direct script access allowed');

class Migration_Install_projects_permissions extends Migration
{
	/**
	 * @var array Permissions to Migrate
	 */
	private $permissionValues = array(
		array(
			'name' => 'projects.Newcontext.View',
			'description' => 'View projects Newcontext',
			'status' => 'active',
		),
		array(
			'name' => 'projects.Newcontext.Create',
			'description' => 'Create projects Newcontext',
			'status' => 'active',
		),
		array(
			'name' => 'projects.Newcontext.Edit',
			'description' => 'Edit projects Newcontext',
			'status' => 'active',
		),
		array(
			'name' => 'projects.Newcontext.Delete',
			'description' => 'Delete projects Newcontext',
			'status' => 'active',
		),
		array(
			'name' => 'projects.Content.View',
			'description' => 'View projects Content',
			'status' => 'active',
		),
		array(
			'name' => 'projects.Content.Create',
			'description' => 'Create projects Content',
			'status' => 'active',
		),
		array(
			'name' => 'projects.Content.Edit',
			'description' => 'Edit projects Content',
			'status' => 'active',
		),
		array(
			'name' => 'projects.Content.Delete',
			'description' => 'Delete projects Content',
			'status' => 'active',
		),
		array(
			'name' => 'projects.Reports.View',
			'description' => 'View projects Reports',
			'status' => 'active',
		),
		array(
			'name' => 'projects.Reports.Create',
			'description' => 'Create projects Reports',
			'status' => 'active',
		),
		array(
			'name' => 'projects.Reports.Edit',
			'description' => 'Edit projects Reports',
			'status' => 'active',
		),
		array(
			'name' => 'projects.Reports.Delete',
			'description' => 'Delete projects Reports',
			'status' => 'active',
		),
		array(
			'name' => 'projects.Settings.View',
			'description' => 'View projects Settings',
			'status' => 'active',
		),
		array(
			'name' => 'projects.Settings.Create',
			'description' => 'Create projects Settings',
			'status' => 'active',
		),
		array(
			'name' => 'projects.Settings.Edit',
			'description' => 'Edit projects Settings',
			'status' => 'active',
		),
		array(
			'name' => 'projects.Settings.Delete',
			'description' => 'Delete projects Settings',
			'status' => 'active',
		),
		array(
			'name' => 'projects.Developer.View',
			'description' => 'View projects Developer',
			'status' => 'active',
		),
		array(
			'name' => 'projects.Developer.Create',
			'description' => 'Create projects Developer',
			'status' => 'active',
		),
		array(
			'name' => 'projects.Developer.Edit',
			'description' => 'Edit projects Developer',
			'status' => 'active',
		),
		array(
			'name' => 'projects.Developer.Delete',
			'description' => 'Delete projects Developer',
			'status' => 'active',
		),
    );

    /**
     * @var string The name of the permission key in the role_permissions table
     */
    private $permissionKey = 'permission_id';

    /**
     * @var string The name of the permission name field in the permissions table
     */
    private $permissionNameField = 'name';

	/**
	 * @var string The name of the role/permissions ref table
	 */
	private $rolePermissionsTable = 'role_permissions';

    /**
     * @var numeric The role id to which the permissions will be applied
     */
    private $roleId = '1';

    /**
     * @var string The name of the role key in the role_permissions table
     */
    private $roleKey = 'role_id';

	/**
	 * @var string The name of the permissions table
	 */
	private $tableName = 'permissions';

	//--------------------------------------------------------------------

	/**
	 * Install this version
	 *
	 * @return void
	 */
	public function up()
	{
		$rolePermissionsData = array();
		foreach ($this->permissionValues as $permissionValue) {
			$this->db->insert($this->tableName, $permissionValue);

			$rolePermissionsData[] = array(
                $this->roleKey       => $this->roleId,
                $this->permissionKey => $this->db->insert_id(),
			);
		}

		$this->db->insert_batch($this->rolePermissionsTable, $rolePermissionsData);
	}

	/**
	 * Uninstall this version
	 *
	 * @return void
	 */
	public function down()
	{
        $permissionNames = array();
		foreach ($this->permissionValues as $permissionValue) {
            $permissionNames[] = $permissionValue[$this->permissionNameField];
        }

        $query = $this->db->select($this->permissionKey)
                          ->where_in($this->permissionNameField, $permissionNames)
                          ->get($this->tableName);

        if ( ! $query->num_rows()) {
            return;
        }

        $permissionIds = array();
        foreach ($query->result() as $row) {
            $permissionIds[] = $row->{$this->permissionKey};
        }

        $this->db->where_in($this->permissionKey, $permissionIds)
                 ->delete($this->rolePermissionsTable);

        $this->db->where_in($this->permissionNameField, $permissionNames)
                 ->delete($this->tableName);
	}
}