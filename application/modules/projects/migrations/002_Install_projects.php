<?php defined('BASEPATH') || exit('No direct script access allowed');

class Migration_Install_projects extends Migration
{
	/**
	 * @var string The name of the database table
	 */
	private $table_name = 'projects';

	/**
	 * @var array The table's fields
	 */
	private $fields = array(
		'id' => array(
			'type'       => 'INT',
			'constraint' => 11,
			'auto_increment' => true,
		),
        'name' => array(
            'type'       => 'VARCHAR',
            'constraint' => 255,
            'null'       => false,
        ),
        'slogan' => array(
            'type'       => 'VARCHAR',
            'constraint' => 255,
            'null'       => true,
        ),
        'description' => array(
            'type'       => 'TEXT',
            'null'       => false,
        ),
        'img' => array(
            'type'       => 'VARCHAR',
            'constraint' => 255,
            'null'       => false,
        ),
        'jobs' => array(
            'type'       => 'INT',
            'constraint' => 3,
            'null'       => true,
        ),
        'project_length' => array(
            'type'       => 'INT',
            'constraint' => 3,
            'null'       => true,
        ),
        'learning' => array(
            'type'       => 'TINYINT',
            'null'       => true,
        ),
        'support' => array(
            'type'       => 'TINYINT',
            'null'       => true,
        ),
	);

	/**
	 * Install this version
	 *
	 * @return void
	 */
	public function up()
	{
		$this->dbforge->add_field($this->fields);
		$this->dbforge->add_key('id', true);
		$this->dbforge->create_table($this->table_name);
	}

	/**
	 * Uninstall this version
	 *
	 * @return void
	 */
	public function down()
	{
		$this->dbforge->drop_table($this->table_name);
	}
}