<?php defined('BASEPATH') || exit('No direct script access allowed');

$lang['reviews_manage']      = 'Gestionar Reviews';
$lang['reviews_edit']        = 'Editar';
$lang['reviews_true']        = 'Verdadero';
$lang['reviews_false']       = 'Falso';
$lang['reviews_create']      = 'Crear';
$lang['reviews_list']        = 'Listar';
$lang['reviews_new']       = 'Nuevo';
$lang['reviews_edit_text']     = 'Editar esto para satisfacer sus necesidades';
$lang['reviews_no_records']    = 'Hay ninguna reviews en la sistema.';
$lang['reviews_create_new']    = 'Crear nuevo(a) Reviews.';
$lang['reviews_create_success']  = 'Reviews creado(a) con éxito.';
$lang['reviews_create_failure']  = 'Hubo un problema al crear el(la) reviews: ';
$lang['reviews_create_new_button'] = 'Crear nuevo(a) Reviews';
$lang['reviews_invalid_id']    = 'ID de Reviews inválido(a).';
$lang['reviews_edit_success']    = 'Reviews guardado correctamente.';
$lang['reviews_edit_failure']    = 'Hubo un problema guardando el(la) reviews: ';
$lang['reviews_delete_success']  = 'Registro(s) eliminado con éxito.';
$lang['reviews_delete_failure']  = 'No hemos podido eliminar el registro: ';
$lang['reviews_delete_error']    = 'No ha seleccionado ning&#250;n registro que desea eliminar.';
$lang['reviews_actions']     = 'Açciones';
$lang['reviews_cancel']      = 'Cancelar';
$lang['reviews_delete_record']   = 'Eliminar este(a) Reviews';
$lang['reviews_delete_confirm']  = '¿Esta seguro de que desea eliminar este(a) reviews?';
$lang['reviews_edit_heading']    = 'Editar Reviews';

// Create/Edit Buttons
$lang['reviews_action_edit']   = 'Guardar Reviews';
$lang['reviews_action_create']   = 'Crear Reviews';

// Activities
$lang['reviews_act_create_record'] = 'Creado registro con ID';
$lang['reviews_act_edit_record'] = 'Actualizado registro con ID';
$lang['reviews_act_delete_record'] = 'Eliminado registro con ID';

//Listing Specifics
$lang['reviews_records_empty']    = 'No hay registros encontrados para su selección.';
$lang['reviews_errors_message']    = 'Por favor corrija los siguientes errores:';

// Column Headings
$lang['reviews_column_created']  = 'Creado';
$lang['reviews_column_deleted']  = 'Elíminado';
$lang['reviews_column_modified'] = 'Modificado';

// Module Details
$lang['reviews_module_name'] = 'Reviews';
$lang['reviews_module_description'] = 'Модуль добаления отзывов о нас';
$lang['reviews_area_title'] = 'Reviews';

// Fields
$lang['reviews_field_from_company'] = 'From company';
$lang['reviews_field_text'] = 'Text';
