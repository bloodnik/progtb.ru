<?php defined('BASEPATH') || exit('No direct script access allowed');


$lang['reviews_manage']      = 'Manage Reviews';
$lang['reviews_edit']        = 'Edit';
$lang['reviews_true']        = 'True';
$lang['reviews_false']       = 'False';
$lang['reviews_create']      = 'Create';
$lang['reviews_list']        = 'List';
$lang['reviews_new']       = 'New';
$lang['reviews_edit_text']     = 'Edit this to suit your needs';
$lang['reviews_no_records']    = 'There are no reviews in the system.';
$lang['reviews_create_new']    = 'Create a new Reviews.';
$lang['reviews_create_success']  = 'Reviews successfully created.';
$lang['reviews_create_failure']  = 'There was a problem creating the reviews: ';
$lang['reviews_create_new_button'] = 'Create New Reviews';
$lang['reviews_invalid_id']    = 'Invalid Reviews ID.';
$lang['reviews_edit_success']    = 'Reviews successfully saved.';
$lang['reviews_edit_failure']    = 'There was a problem saving the reviews: ';
$lang['reviews_delete_success']  = 'record(s) successfully deleted.';
$lang['reviews_delete_failure']  = 'We could not delete the record: ';
$lang['reviews_delete_error']    = 'You have not selected any records to delete.';
$lang['reviews_actions']     = 'Actions';
$lang['reviews_cancel']      = 'Cancel';
$lang['reviews_delete_record']   = 'Delete this Reviews';
$lang['reviews_delete_confirm']  = 'Are you sure you want to delete this reviews?';
$lang['reviews_edit_heading']    = 'Edit Reviews';

// Create/Edit Buttons
$lang['reviews_action_edit']   = 'Save Reviews';
$lang['reviews_action_create']   = 'Create Reviews';

// Activities
$lang['reviews_act_create_record'] = 'Created record with ID';
$lang['reviews_act_edit_record'] = 'Updated record with ID';
$lang['reviews_act_delete_record'] = 'Deleted record with ID';

//Listing Specifics
$lang['reviews_records_empty']    = 'No records found that match your selection.';
$lang['reviews_errors_message']    = 'Please fix the following errors:';

// Column Headings
$lang['reviews_column_created']  = 'Created';
$lang['reviews_column_deleted']  = 'Deleted';
$lang['reviews_column_modified'] = 'Modified';
$lang['reviews_column_deleted_by'] = 'Deleted By';
$lang['reviews_column_created_by'] = 'Created By';
$lang['reviews_column_modified_by'] = 'Modified By';

// Module Details
$lang['reviews_module_name'] = 'Reviews';
$lang['reviews_module_description'] = 'Модуль добаления отзывов о нас';
$lang['reviews_area_title'] = 'Reviews';

// Fields
$lang['reviews_field_from_company'] = 'From company';
$lang['reviews_field_text'] = 'Text';