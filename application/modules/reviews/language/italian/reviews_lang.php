<?php defined('BASEPATH') || exit('No direct script access allowed');

$lang['reviews_manage']      = 'Gestisci Reviews';
$lang['reviews_edit']        = 'Modifica';
$lang['reviews_true']        = 'Vero';
$lang['reviews_false']       = 'Falso';
$lang['reviews_create']      = 'Crea';
$lang['reviews_list']        = 'Elenca';
$lang['reviews_new']       = 'Nuovo';
$lang['reviews_edit_text']     = 'Modifica questo secondo le tue necessità';
$lang['reviews_no_records']    = 'Non ci sono reviews nel sistema.';
$lang['reviews_create_new']    = 'Crea un nuovo Reviews.';
$lang['reviews_create_success']  = 'Reviews creato con successo.';
$lang['reviews_create_failure']  = 'C\'è stato un problema nella creazione di reviews: ';
$lang['reviews_create_new_button'] = 'Crea nuovo Reviews';
$lang['reviews_invalid_id']    = 'ID Reviews non valido.';
$lang['reviews_edit_success']    = 'Reviews creato con successo.';
$lang['reviews_edit_failure']    = 'C\'è stato un errore nel salvataggio di reviews: ';
$lang['reviews_delete_success']  = 'record(s) creati con successo.';
$lang['reviews_delete_failure']  = 'Non possiamo eliminare il record: ';
$lang['reviews_delete_error']    = 'Non hai selezionato alcun record da eliminare.';
$lang['reviews_actions']     = 'Azioni';
$lang['reviews_cancel']      = 'Cancella';
$lang['reviews_delete_record']   = 'Elimina questo Reviews';
$lang['reviews_delete_confirm']  = 'Sei sicuro di voler eliminare questo reviews?';
$lang['reviews_edit_heading']    = 'Modifica Reviews';

// Create/Edit Buttons
$lang['reviews_action_edit']   = 'Salva Reviews';
$lang['reviews_action_create']   = 'Crea Reviews';

// Activities
$lang['reviews_act_create_record'] = 'Creato il record con ID';
$lang['reviews_act_edit_record'] = 'Aggiornato il record con ID';
$lang['reviews_act_delete_record'] = 'Eliminato il record con ID';

// Listing Specifics
$lang['reviews_records_empty']    = 'Nessun record trovato che corrisponda alla tua selezione.';
$lang['reviews_errors_message']    = 'Per favore risolvi i seguenti errori:';

// Column Headings
$lang['reviews_column_created']  = 'Creato';
$lang['reviews_column_deleted']  = 'Eliminato';
$lang['reviews_column_modified'] = 'Modificato';

// Module Details
$lang['reviews_module_name'] = 'Reviews';
$lang['reviews_module_description'] = 'Модуль добаления отзывов о нас';
$lang['reviews_area_title'] = 'Reviews';

// Fields
$lang['reviews_field_from_company'] = 'From company';
$lang['reviews_field_text'] = 'Text';
