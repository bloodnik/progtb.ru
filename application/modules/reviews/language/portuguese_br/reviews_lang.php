<?php defined('BASEPATH') || exit('No direct script access allowed');

$lang['reviews_manage']      = 'Gerenciar Reviews';
$lang['reviews_edit']        = 'Editar';
$lang['reviews_true']        = 'Verdadeiro';
$lang['reviews_false']       = 'Falso';
$lang['reviews_create']      = 'Criar';
$lang['reviews_list']        = 'Listar';
$lang['reviews_new']       = 'Novo';
$lang['reviews_edit_text']     = 'Edite isto conforme sua necessidade';
$lang['reviews_no_records']    = 'Não há reviews no sistema.';
$lang['reviews_create_new']    = 'Criar novo(a) Reviews.';
$lang['reviews_create_success']  = 'Reviews Criado(a) com sucesso.';
$lang['reviews_create_failure']  = 'Ocorreu um problema criando o(a) reviews: ';
$lang['reviews_create_new_button'] = 'Criar novo(a) Reviews';
$lang['reviews_invalid_id']    = 'ID de Reviews inválida.';
$lang['reviews_edit_success']    = 'Reviews salvo(a) com sucesso.';
$lang['reviews_edit_failure']    = 'Ocorreu um problema salvando o(a) reviews: ';
$lang['reviews_delete_success']  = 'Registro(s) excluído(s) com sucesso.';
$lang['reviews_delete_failure']  = 'Não foi possível excluir o registro: ';
$lang['reviews_delete_error']    = 'Voc6e não selecionou nenhum registro para excluir.';
$lang['reviews_actions']     = 'Ações';
$lang['reviews_cancel']      = 'Cancelar';
$lang['reviews_delete_record']   = 'Excluir este(a) Reviews';
$lang['reviews_delete_confirm']  = 'Você tem certeza que deseja excluir este(a) reviews?';
$lang['reviews_edit_heading']    = 'Editar Reviews';

// Create/Edit Buttons
$lang['reviews_action_edit']   = 'Salvar Reviews';
$lang['reviews_action_create']   = 'Criar Reviews';

// Activities
$lang['reviews_act_create_record'] = 'Criado registro com ID';
$lang['reviews_act_edit_record'] = 'Atualizado registro com ID';
$lang['reviews_act_delete_record'] = 'Excluído registro com ID';

//Listing Specifics
$lang['reviews_records_empty']    = 'Nenhum registro encontrado.';
$lang['reviews_errors_message']    = 'Por favor corrija os erros a seguir:';

// Column Headings
$lang['reviews_column_created']  = 'Criado';
$lang['reviews_column_deleted']  = 'Excluído';
$lang['reviews_column_modified'] = 'Atualizado';

// Module Details
$lang['reviews_module_name'] = 'Reviews';
$lang['reviews_module_description'] = 'Модуль добаления отзывов о нас';
$lang['reviews_area_title'] = 'Reviews';

// Fields
$lang['reviews_field_from_company'] = 'From company';
$lang['reviews_field_text'] = 'Text';
