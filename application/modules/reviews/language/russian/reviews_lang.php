<?php defined('BASEPATH') || exit('No direct script access allowed');


$lang['reviews_manage']      = 'Управление отзывами';
$lang['reviews_edit']        = 'Редактировать';
$lang['reviews_true']        = 'Да';
$lang['reviews_false']       = 'Нет';
$lang['reviews_create']      = 'Создать';
$lang['reviews_list']        = 'Список отзывов';
$lang['reviews_new']       = 'Новый элемент';
$lang['reviews_edit_text']     = 'Отредактируте элемент если вам это необходимо';
$lang['reviews_no_records']    = 'Нет ни одной записи.';
$lang['reviews_create_new']    = 'Добавить новый элемент.';
$lang['reviews_create_success']  = 'Элемент успешно добавлен.';
$lang['reviews_create_failure']  = 'Есть некоторые проблемы при создании: ';
$lang['reviews_create_new_button'] = 'Создать новые элементы';
$lang['reviews_invalid_id']    = 'Неверный ID элемента.';
$lang['reviews_edit_success']    = 'Элемент успешно сохранен.';
$lang['reviews_edit_failure']    = 'Есть проблемы при сохранении: ';
$lang['reviews_delete_success']  = 'Запись(и) успешно удалена.';
$lang['reviews_delete_failure']  = 'Не возможно удалить элемент: ';
$lang['reviews_delete_error']    = 'Не выбран ни один элемент для удаления.';
$lang['reviews_actions']     = 'Действия';
$lang['reviews_cancel']      = 'Отмена';
$lang['reviews_delete_record']   = 'Удалить этот элемент';
$lang['reviews_delete_confirm']  = 'Вы действительно хотите удалить этот элемент?';
$lang['reviews_edit_heading']    = 'Редактирование элемента';

// Create/Edit Buttons
$lang['reviews_action_edit']   = 'Сохранить элемент';
$lang['reviews_action_create']   = 'Создать элемент';

// Activities
$lang['reviews_act_create_record'] = 'Создать запись с ID';
$lang['reviews_act_edit_record'] = 'Обновить запись с ID';
$lang['reviews_act_delete_record'] = 'Удалить запись с ID';

//Listing Specifics
$lang['reviews_records_empty']    = 'Не найдены удовлетворяющие вас записи.';
$lang['reviews_errors_message']    = 'Пожалуйста, исправте следующие проблемы:';

// Column Headings
$lang['reviews_column_created']  = 'Создано';
$lang['reviews_column_deleted']  = 'Удалено';
$lang['reviews_column_modified'] = 'Обновлено';
$lang['reviews_column_deleted_by'] = 'Удалено ';
$lang['reviews_column_created_by'] = 'Создано';
$lang['reviews_column_modified_by'] = 'Обновлено';

// Module Details
$lang['reviews_module_name'] = 'Элементы';
$lang['reviews_module_description'] = 'Отзывы';
$lang['reviews_area_title'] = 'Отзывы';


// Fields
$lang['reviews_field_from_company'] = 'От компании';
$lang['reviews_field_text'] = "Отзыв";