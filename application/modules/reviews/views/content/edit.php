<?php

$validation_errors = validation_errors();

if ($validation_errors) :
?>
<div class='alert alert-block alert-error fade in'>
	<a class='close' data-dismiss='alert'>&times;</a>
	<h4 class='alert-heading'>
		<?php echo lang('reviews_errors_message'); ?>
	</h4>
	<?php echo $validation_errors; ?>
</div>
<?php
endif;

$id = isset($reviews->id) ? $reviews->id : '';

?>
<div class='admin-box'>
	<h3>Добавление отзыва</h3>
	<?php echo form_open($this->uri->uri_string(), 'class="form-horizontal"'); ?>
		<fieldset>
            

			<div class="control-group<?php echo form_error('from_company') ? ' error' : ''; ?>">
				<?php echo form_label(lang('reviews_field_from_company'). lang('bf_form_label_required'), 'from_company', array('class' => 'control-label')); ?>
				<div class='controls'>
					<input id='from_company' type='text' required='required' name='from_company' maxlength='125' value="<?php echo set_value('from_company', isset($reviews->from_company) ? $reviews->from_company : ''); ?>" />
					<span class='help-inline'><?php echo form_error('from_company'); ?></span>
				</div>
			</div>

			<div class="control-group<?php echo form_error('text') ? ' error' : ''; ?>">
				<?php echo form_label(lang('reviews_field_text'). lang('bf_form_label_required'), 'text', array('class' => 'control-label')); ?>
				<div class='controls'>
					<?php echo form_textarea(array('name' => 'text', 'id' => 'text', 'rows' => '5', 'cols' => '80', 'value' => set_value('text', isset($reviews->text) ? $reviews->text : ''), 'required' => 'required')); ?>
					<span class='help-inline'><?php echo form_error('text'); ?></span>
				</div>
			</div>
        </fieldset>
		<fieldset class='form-actions'>
			<input type='submit' name='save' class='btn btn-primary' value="<?php echo lang('reviews_action_edit'); ?>" />
			<?php echo lang('bf_or'); ?>
			<?php echo anchor(SITE_AREA . '/content/reviews', lang('reviews_cancel'), 'class="btn btn-warning"'); ?>
			
			<?php if ($this->auth->has_permission('Reviews.Content.Delete')) : ?>
				<?php echo lang('bf_or'); ?>
				<button type='submit' name='delete' formnovalidate class='btn btn-danger' id='delete-me' onclick="return confirm('<?php e(js_escape(lang('reviews_delete_confirm'))); ?>');">
					<span class='icon-trash icon-white'></span>&nbsp;<?php echo lang('reviews_delete_record'); ?>
				</button>
			<?php endif; ?>
		</fieldset>
    <?php echo form_close(); ?>
</div>