
    var xinha_plugins = ['Linker'],
        xinha_editors = ['text'];

    function xinha_init() {
        if ( ! Xinha.loadPlugins(xinha_plugins, xinha_init)) {
            return;
        }

        var xinha_config = new Xinha.Config();
        xinha_editors = Xinha.makeEditors(xinha_editors, xinha_config, xinha_plugins);
        Xinha.startEditors(xinha_editors);
    }

    xinha_init();