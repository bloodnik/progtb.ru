<?php defined('BASEPATH') || exit('No direct script access allowed');

/**
 * Content controller
 */
class Content extends Admin_Controller
{
    protected $permissionCreate = 'Reviews.Content.Create';
    protected $permissionDelete = 'Reviews.Content.Delete';
    protected $permissionEdit   = 'Reviews.Content.Edit';
    protected $permissionView   = 'Reviews.Content.View';

    /**
	 * Constructor
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
		
        $this->auth->restrict($this->permissionView);
		$this->load->model('reviews/reviews_model');
        $this->lang->load('reviews');
		
			Assets::add_js(Template::theme_url('js/editors/xinha_conf.js'));
			Assets::add_js(Template::theme_url('js/editors/xinha/XinhaCore.js'));
            $this->form_validation->set_error_delimiters("<span class='error'>", "</span>");
        
		Template::set_block('sub_nav', 'content/_sub_nav');

		Assets::add_module_js('reviews', 'reviews.js');
	}

	/**
	 * Display a list of Reviews data.
	 *
	 * @return void
	 */
	public function index($offset = 0)
	{
        // Deleting anything?
		if (isset($_POST['delete'])) {
            $this->auth->restrict($this->permissionDelete);
			$checked = $this->input->post('checked');
			if (is_array($checked) && count($checked)) {

                // If any of the deletions fail, set the result to false, so
                // failure message is set if any of the attempts fail, not just
                // the last attempt

				$result = true;
				foreach ($checked as $pid) {
					$deleted = $this->reviews_model->delete($pid);
                    if ($deleted == false) {
                        $result = false;
                    }
				}
				if ($result) {
					Template::set_message(count($checked) . ' ' . lang('reviews_delete_success'), 'success');
				} else {
					Template::set_message(lang('reviews_delete_failure') . $this->reviews_model->error, 'error');
				}
			}
		}
        $pagerUriSegment = 5;
        $pagerBaseUrl = site_url(SITE_AREA . '/content/reviews/index') . '/';
        
        $limit  = $this->settings_lib->item('site.list_limit') ?: 15;

        $this->load->library('pagination');
        $pager['base_url']    = $pagerBaseUrl;
        $pager['total_rows']  = $this->reviews_model->count_all();
        $pager['per_page']    = $limit;
        $pager['uri_segment'] = $pagerUriSegment;

        $this->pagination->initialize($pager);
        $this->reviews_model->limit($limit, $offset);
        
		$records = $this->reviews_model->find_all();

		Template::set('records', $records);
        
    Template::set('toolbar_title', lang('reviews_manage'));

		Template::render();
	}
    
    /**
	 * Create a Reviews object.
	 *
	 * @return void
	 */
	public function create()
	{
		$this->auth->restrict($this->permissionCreate);
        
		if (isset($_POST['save'])) {
			if ($insert_id = $this->save_reviews()) {
				log_activity($this->auth->user_id(), lang('reviews_act_create_record') . ': ' . $insert_id . ' : ' . $this->input->ip_address(), 'reviews');
				Template::set_message(lang('reviews_create_success'), 'success');

				redirect(SITE_AREA . '/content/reviews');
			}

            // Not validation error
			if ( ! empty($this->reviews_model->error)) {
				Template::set_message(lang('reviews_create_failure') . $this->reviews_model->error, 'error');
            }
		}

		Template::set('toolbar_title', lang('reviews_action_create'));

		Template::render();
	}
	/**
	 * Allows editing of Reviews data.
	 *
	 * @return void
	 */
	public function edit()
	{
		$id = $this->uri->segment(5);
		if (empty($id)) {
			Template::set_message(lang('reviews_invalid_id'), 'error');

			redirect(SITE_AREA . '/content/reviews');
		}
        
		if (isset($_POST['save'])) {
			$this->auth->restrict($this->permissionEdit);

			if ($this->save_reviews('update', $id)) {
				log_activity($this->auth->user_id(), lang('reviews_act_edit_record') . ': ' . $id . ' : ' . $this->input->ip_address(), 'reviews');
				Template::set_message(lang('reviews_edit_success'), 'success');
				redirect(SITE_AREA . '/content/reviews');
			}

            // Not validation error
            if ( ! empty($this->reviews_model->error)) {
                Template::set_message(lang('reviews_edit_failure') . $this->reviews_model->error, 'error');
			}
		}
        
		elseif (isset($_POST['delete'])) {
			$this->auth->restrict($this->permissionDelete);

			if ($this->reviews_model->delete($id)) {
				log_activity($this->auth->user_id(), lang('reviews_act_delete_record') . ': ' . $id . ' : ' . $this->input->ip_address(), 'reviews');
				Template::set_message(lang('reviews_delete_success'), 'success');

				redirect(SITE_AREA . '/content/reviews');
			}

            Template::set_message(lang('reviews_delete_failure') . $this->reviews_model->error, 'error');
		}
        
        Template::set('reviews', $this->reviews_model->find($id));

		Template::set('toolbar_title', lang('reviews_edit_heading'));
		Template::render();
	}

	//--------------------------------------------------------------------
	// !PRIVATE METHODS
	//--------------------------------------------------------------------

	/**
	 * Save the data.
	 *
	 * @param string $type Either 'insert' or 'update'.
	 * @param int	 $id	The ID of the record to update, ignored on inserts.
	 *
	 * @return bool|int An int ID for successful inserts, true for successful
     * updates, else false.
	 */
	private function save_reviews($type = 'insert', $id = 0)
	{
		if ($type == 'update') {
			$_POST['id'] = $id;
		}

        // Validate the data
        $this->form_validation->set_rules($this->reviews_model->get_validation_rules());
        if ($this->form_validation->run() === false) {
            return false;
        }

		// Make sure we only pass in the fields we want
		
		$data = $this->reviews_model->prep_data($this->input->post());

        // Additional handling for default values should be added below,
        // or in the model's prep_data() method
        

        $return = false;
		if ($type == 'insert') {
			$id = $this->reviews_model->insert($data);

			if (is_numeric($id)) {
				$return = $id;
			}
		} elseif ($type == 'update') {
			$return = $this->reviews_model->update($id, $data);
		}

		return $return;
	}
}