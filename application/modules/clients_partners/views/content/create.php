<?php

$validation_errors = validation_errors();

if ($validation_errors) :
?>
<div class='alert alert-block alert-error fade in'>
	<a class='close' data-dismiss='alert'>&times;</a>
	<h4 class='alert-heading'>
		<?php echo lang('clients_partners_errors_message'); ?>
	</h4>
	<?php echo $validation_errors; ?>
</div>
<?php
endif;

$id = isset($clients_partners->id) ? $clients_partners->id : '';

?>
<div class='admin-box'>
	<h3>Добавить клиента(партнера)</h3>
	<?php echo form_open_multipart($this->uri->uri_string(), 'class="form-horizontal"'); ?>
		<fieldset>
            

			<div class="control-group<?php echo form_error('name') ? ' error' : ''; ?>">
				<?php echo form_label(lang('clients_partners_field_name'). lang('bf_form_label_required'), 'name', array('class' => 'control-label')); ?>
				<div class='controls'>
					<input id='name' type='text' required='required' name='name' maxlength='125' value="<?php echo set_value('name', isset($clients_partners->name) ? $clients_partners->name : ''); ?>" />
					<span class='help-inline'><?php echo form_error('name'); ?></span>
				</div>
			</div>
            <div class="control-group<?php echo form_error('link') ? ' error' : ''; ?>">
                <?php echo form_label(lang('clients_partners_field_link'), 'link', array('class' => 'control-label')); ?>
                <div class='controls'>
                    <input id='link' type='text' name='link' maxlength='125' value="<?php echo set_value('link', isset($clients_partners->link) ? $clients_partners->link : ''); ?>" />
                    <span class='help-inline'><?php echo form_error('link'); ?></span>
                </div>
            </div>
			<div class="control-group<?php echo form_error('image') ? ' error' : ''; ?>">
				<?php echo form_label(lang('clients_partners_field_image'). lang('bf_form_label_required'), 'image', array('class' => 'control-label')); ?>
				<div class='controls'>
					<input id='image' type='file' required='required' name='image' maxlength='200' value="<?php echo set_value('image', isset($clients_partners->image) ? $clients_partners->image : ''); ?>" />
					<span class='help-inline'><?php echo form_error('image'); ?></span>
				</div>
			</div>

			<div class="control-group<?php echo form_error('partner') ? ' error' : ''; ?>">
				<div class='controls'>
					<label class='checkbox' for='partner'>
						<input type='checkbox' id='partner' name='partner'  value='1' <?php echo set_checkbox('partner', 1, isset($clients_partners->partner) && $clients_partners->partner == 1); ?> />
                        <?=lang('clients_partners_field_partner')?>
					</label>
                    <span class='help-inline'><?php echo form_error('partner'); ?></span>
				</div>
			</div>
        </fieldset>
		<fieldset class='form-actions'>
			<input type='submit' name='save' class='btn btn-primary' value="<?php echo lang('clients_partners_action_create'); ?>" />
			<?php echo lang('bf_or'); ?>
			<?php echo anchor(SITE_AREA . '/content/clients_partners', lang('clients_partners_cancel'), 'class="btn btn-warning"'); ?>
			
		</fieldset>
    <?php echo form_close(); ?>
</div>