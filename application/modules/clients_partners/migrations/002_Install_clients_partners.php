<?php defined('BASEPATH') || exit('No direct script access allowed');

class Migration_Install_clients_partners extends Migration
{
	/**
	 * @var string The name of the database table
	 */
	private $table_name = 'clients_partners';

	/**
	 * @var array The table's fields
	 */
	private $fields = array(
		'id' => array(
			'type'       => 'INT',
			'constraint' => 11,
			'auto_increment' => true,
		),
        'name' => array(
            'type'       => 'VARCHAR',
            'constraint' => 125,
            'null'       => false,
        ),
        'image' => array(
            'type'       => 'VARCHAR',
            'constraint' => 200,
            'null'       => false,
        ),
        'partner' => array(
            'type'       => 'TINYINT',
            'null'       => true,
        ),
	);

	/**
	 * Install this version
	 *
	 * @return void
	 */
	public function up()
	{
		$this->dbforge->add_field($this->fields);
		$this->dbforge->add_key('id', true);
		$this->dbforge->create_table($this->table_name);
	}

	/**
	 * Uninstall this version
	 *
	 * @return void
	 */
	public function down()
	{
		$this->dbforge->drop_table($this->table_name);
	}
}