<?php defined('BASEPATH') || exit('No direct script access allowed');

class Migration_Install_clients_partners_permissions extends Migration
{
	/**
	 * @var array Permissions to Migrate
	 */
	private $permissionValues = array(
		array(
			'name' => 'Clients_partners.Content.View',
			'description' => 'View Clients_partners Content',
			'status' => 'active',
		),
		array(
			'name' => 'Clients_partners.Content.Create',
			'description' => 'Create Clients_partners Content',
			'status' => 'active',
		),
		array(
			'name' => 'Clients_partners.Content.Edit',
			'description' => 'Edit Clients_partners Content',
			'status' => 'active',
		),
		array(
			'name' => 'Clients_partners.Content.Delete',
			'description' => 'Delete Clients_partners Content',
			'status' => 'active',
		),
		array(
			'name' => 'Clients_partners.Reports.View',
			'description' => 'View Clients_partners Reports',
			'status' => 'active',
		),
		array(
			'name' => 'Clients_partners.Reports.Create',
			'description' => 'Create Clients_partners Reports',
			'status' => 'active',
		),
		array(
			'name' => 'Clients_partners.Reports.Edit',
			'description' => 'Edit Clients_partners Reports',
			'status' => 'active',
		),
		array(
			'name' => 'Clients_partners.Reports.Delete',
			'description' => 'Delete Clients_partners Reports',
			'status' => 'active',
		),
		array(
			'name' => 'Clients_partners.Settings.View',
			'description' => 'View Clients_partners Settings',
			'status' => 'active',
		),
		array(
			'name' => 'Clients_partners.Settings.Create',
			'description' => 'Create Clients_partners Settings',
			'status' => 'active',
		),
		array(
			'name' => 'Clients_partners.Settings.Edit',
			'description' => 'Edit Clients_partners Settings',
			'status' => 'active',
		),
		array(
			'name' => 'Clients_partners.Settings.Delete',
			'description' => 'Delete Clients_partners Settings',
			'status' => 'active',
		),
		array(
			'name' => 'Clients_partners.Developer.View',
			'description' => 'View Clients_partners Developer',
			'status' => 'active',
		),
		array(
			'name' => 'Clients_partners.Developer.Create',
			'description' => 'Create Clients_partners Developer',
			'status' => 'active',
		),
		array(
			'name' => 'Clients_partners.Developer.Edit',
			'description' => 'Edit Clients_partners Developer',
			'status' => 'active',
		),
		array(
			'name' => 'Clients_partners.Developer.Delete',
			'description' => 'Delete Clients_partners Developer',
			'status' => 'active',
		),
    );

    /**
     * @var string The name of the permission key in the role_permissions table
     */
    private $permissionKey = 'permission_id';

    /**
     * @var string The name of the permission name field in the permissions table
     */
    private $permissionNameField = 'name';

	/**
	 * @var string The name of the role/permissions ref table
	 */
	private $rolePermissionsTable = 'role_permissions';

    /**
     * @var numeric The role id to which the permissions will be applied
     */
    private $roleId = '1';

    /**
     * @var string The name of the role key in the role_permissions table
     */
    private $roleKey = 'role_id';

	/**
	 * @var string The name of the permissions table
	 */
	private $tableName = 'permissions';

	//--------------------------------------------------------------------

	/**
	 * Install this version
	 *
	 * @return void
	 */
	public function up()
	{
		$rolePermissionsData = array();
		foreach ($this->permissionValues as $permissionValue) {
			$this->db->insert($this->tableName, $permissionValue);

			$rolePermissionsData[] = array(
                $this->roleKey       => $this->roleId,
                $this->permissionKey => $this->db->insert_id(),
			);
		}

		$this->db->insert_batch($this->rolePermissionsTable, $rolePermissionsData);
	}

	/**
	 * Uninstall this version
	 *
	 * @return void
	 */
	public function down()
	{
        $permissionNames = array();
		foreach ($this->permissionValues as $permissionValue) {
            $permissionNames[] = $permissionValue[$this->permissionNameField];
        }

        $query = $this->db->select($this->permissionKey)
                          ->where_in($this->permissionNameField, $permissionNames)
                          ->get($this->tableName);

        if ( ! $query->num_rows()) {
            return;
        }

        $permissionIds = array();
        foreach ($query->result() as $row) {
            $permissionIds[] = $row->{$this->permissionKey};
        }

        $this->db->where_in($this->permissionKey, $permissionIds)
                 ->delete($this->rolePermissionsTable);

        $this->db->where_in($this->permissionNameField, $permissionNames)
                 ->delete($this->tableName);
	}
}