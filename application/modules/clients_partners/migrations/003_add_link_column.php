<?php defined('BASEPATH') || exit('No direct script access allowed');

class Migration_Add_link_column extends Migration
{
    /**
     * Install this version
     *
     * @return void
     */
    public function up()
    {
        $fields = array(
            'link' => array(
                'type'       => 'VARCHAR',
                'constraint' => 255,
                'null'       => false,
            ),
        );
        $this->dbforge->add_column('clients_partners', $fields, "id");
    }

    /**
     * Uninstall this version
     *
     * @return void
     */
    public function down()
    {
        $this->dbforge->drop_column('clients_partners', "link");
    }
}