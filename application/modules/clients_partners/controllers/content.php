<?php defined('BASEPATH') || exit('No direct script access allowed');

/**
 * Content controller
 */
class Content extends Admin_Controller
{
    protected $permissionCreate = 'Clients_partners.Content.Create';
    protected $permissionDelete = 'Clients_partners.Content.Delete';
    protected $permissionEdit   = 'Clients_partners.Content.Edit';
    protected $permissionView   = 'Clients_partners.Content.View';

    /**
	 * Constructor
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
		
        $this->auth->restrict($this->permissionView);
		$this->load->model('clients_partners/clients_partners_model');
        $this->lang->load('clients_partners');
		
            $this->form_validation->set_error_delimiters("<span class='error'>", "</span>");
        
		Template::set_block('sub_nav', 'content/_sub_nav');

		Assets::add_module_js('clients_partners', 'clients_partners.js');

        $this->load->helper('aiti_file');
	}

	/**
	 * Display a list of Clients-Partners data.
	 *
	 * @return void
	 */
	public function index($offset = 0)
	{
        // Deleting anything?
		if (isset($_POST['delete'])) {
            $this->auth->restrict($this->permissionDelete);
			$checked = $this->input->post('checked');
			if (is_array($checked) && count($checked)) {

                // If any of the deletions fail, set the result to false, so
                // failure message is set if any of the attempts fail, not just
                // the last attempt

				$result = true;
				foreach ($checked as $pid) {
					$deleted = $this->clients_partners_model->delete($pid);
                    if ($deleted == false) {
                        $result = false;
                    }
				}
				if ($result) {
					Template::set_message(count($checked) . ' ' . lang('clients_partners_delete_success'), 'success');
				} else {
					Template::set_message(lang('clients_partners_delete_failure') . $this->clients_partners_model->error, 'error');
				}
			}
		}
        $pagerUriSegment = 5;
        $pagerBaseUrl = site_url(SITE_AREA . '/content/clients_partners/index') . '/';
        
        $limit  = $this->settings_lib->item('site.list_limit') ?: 15;

        $this->load->library('pagination');
        $pager['base_url']    = $pagerBaseUrl;
        $pager['total_rows']  = $this->clients_partners_model->count_all();
        $pager['per_page']    = $limit;
        $pager['uri_segment'] = $pagerUriSegment;

        $this->pagination->initialize($pager);
        $this->clients_partners_model->limit($limit, $offset);
        
		$records = $this->clients_partners_model->find_all();

		Template::set('records', $records);
        
    Template::set('toolbar_title', lang('clients_partners_manage'));

		Template::render();
	}
    
    /**
	 * Create a Clients-Partners object.
	 *
	 * @return void
	 */
	public function create()
	{
		$this->auth->restrict($this->permissionCreate);
        
		if (isset($_POST['save'])) {
			if ($insert_id = $this->save_clients_partners()) {
				log_activity($this->auth->user_id(), lang('clients_partners_act_create_record') . ': ' . $insert_id . ' : ' . $this->input->ip_address(), 'clients_partners');
				Template::set_message(lang('clients_partners_create_success'), 'success');

				redirect(SITE_AREA . '/content/clients_partners');
			}

            // Not validation error
			if ( ! empty($this->clients_partners_model->error)) {
				Template::set_message(lang('clients_partners_create_failure') . $this->clients_partners_model->error, 'error');
            }
		}

		Template::set('toolbar_title', lang('clients_partners_action_create'));

		Template::render();
	}
	/**
	 * Allows editing of Clients-Partners data.
	 *
	 * @return void
	 */
	public function edit()
	{
		$id = $this->uri->segment(5);
		if (empty($id)) {
			Template::set_message(lang('clients_partners_invalid_id'), 'error');

			redirect(SITE_AREA . '/content/clients_partners');
		}
        
		if (isset($_POST['save'])) {
			$this->auth->restrict($this->permissionEdit);

			if ($this->save_clients_partners('update', $id)) {
				log_activity($this->auth->user_id(), lang('clients_partners_act_edit_record') . ': ' . $id . ' : ' . $this->input->ip_address(), 'clients_partners');
				Template::set_message(lang('clients_partners_edit_success'), 'success');
				redirect(SITE_AREA . '/content/clients_partners');
			}

            // Not validation error
            if ( ! empty($this->clients_partners_model->error)) {
                Template::set_message(lang('clients_partners_edit_failure') . $this->clients_partners_model->error, 'error');
			}
		}
        
		elseif (isset($_POST['delete'])) {
			$this->auth->restrict($this->permissionDelete);

			if ($this->clients_partners_model->delete($id)) {
				log_activity($this->auth->user_id(), lang('clients_partners_act_delete_record') . ': ' . $id . ' : ' . $this->input->ip_address(), 'clients_partners');
				Template::set_message(lang('clients_partners_delete_success'), 'success');

				redirect(SITE_AREA . '/content/clients_partners');
			}

            Template::set_message(lang('clients_partners_delete_failure') . $this->clients_partners_model->error, 'error');
		}
        
        Template::set('clients_partners', $this->clients_partners_model->find($id));

		Template::set('toolbar_title', lang('clients_partners_edit_heading'));
		Template::render();
	}

	//--------------------------------------------------------------------
	// !PRIVATE METHODS
	//--------------------------------------------------------------------

	/**
	 * Save the data.
	 *
	 * @param string $type Either 'insert' or 'update'.
	 * @param int	 $id	The ID of the record to update, ignored on inserts.
	 *
	 * @return bool|int An int ID for successful inserts, true for successful
     * updates, else false.
	 */
	private function save_clients_partners($type = 'insert', $id = 0)
	{
		if ($type == 'update') {
			$_POST['id'] = $id;
		}

        // Validate the data
        $this->form_validation->set_rules($this->clients_partners_model->get_validation_rules());
        if ($this->form_validation->run() === false) {
            return false;
        }

		// Make sure we only pass in the fields we want
		
		$data = $this->clients_partners_model->prep_data($this->input->post());

        //Стоит ли галочка "Партнер"
        if (!isset($data["partner"])){
            $data["partner"] = NULL;
        }

        //заливаем изображение и получаем на него ссылку
        $config['upload_path'] = './uploads/partners_img/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size']	= '1000';
        $config['max_width']  = '1024';
        $config['max_height']  = '768';
        $config['encrypt_name']  = TRUE;

        $img_name = uploads_file_to_server($_POST, "image_value", '/uploads/partners_img/', $config, $this);

        $data['image'] = $img_name;

        // Additional handling for default values should be added below,
        // or in the model's prep_data() method
        

        $return = false;
		if ($type == 'insert') {
			$id = $this->clients_partners_model->insert($data);

			if (is_numeric($id)) {
				$return = $id;
			}
		} elseif ($type == 'update') {
			$return = $this->clients_partners_model->update($id, $data);
		}

		return $return;
	}
}