<?php defined('BASEPATH') || exit('No direct script access allowed');

$lang['clients_partners_manage']      = 'Gerenciar Clients-Partners';
$lang['clients_partners_edit']        = 'Editar';
$lang['clients_partners_true']        = 'Verdadeiro';
$lang['clients_partners_false']       = 'Falso';
$lang['clients_partners_create']      = 'Criar';
$lang['clients_partners_list']        = 'Listar';
$lang['clients_partners_new']       = 'Novo';
$lang['clients_partners_edit_text']     = 'Edite isto conforme sua necessidade';
$lang['clients_partners_no_records']    = 'Não há clients_partners no sistema.';
$lang['clients_partners_create_new']    = 'Criar novo(a) Clients-Partners.';
$lang['clients_partners_create_success']  = 'Clients-Partners Criado(a) com sucesso.';
$lang['clients_partners_create_failure']  = 'Ocorreu um problema criando o(a) clients_partners: ';
$lang['clients_partners_create_new_button'] = 'Criar novo(a) Clients-Partners';
$lang['clients_partners_invalid_id']    = 'ID de Clients-Partners inválida.';
$lang['clients_partners_edit_success']    = 'Clients-Partners salvo(a) com sucesso.';
$lang['clients_partners_edit_failure']    = 'Ocorreu um problema salvando o(a) clients_partners: ';
$lang['clients_partners_delete_success']  = 'Registro(s) excluído(s) com sucesso.';
$lang['clients_partners_delete_failure']  = 'Não foi possível excluir o registro: ';
$lang['clients_partners_delete_error']    = 'Voc6e não selecionou nenhum registro para excluir.';
$lang['clients_partners_actions']     = 'Ações';
$lang['clients_partners_cancel']      = 'Cancelar';
$lang['clients_partners_delete_record']   = 'Excluir este(a) Clients-Partners';
$lang['clients_partners_delete_confirm']  = 'Você tem certeza que deseja excluir este(a) clients_partners?';
$lang['clients_partners_edit_heading']    = 'Editar Clients-Partners';

// Create/Edit Buttons
$lang['clients_partners_action_edit']   = 'Salvar Clients-Partners';
$lang['clients_partners_action_create']   = 'Criar Clients-Partners';

// Activities
$lang['clients_partners_act_create_record'] = 'Criado registro com ID';
$lang['clients_partners_act_edit_record'] = 'Atualizado registro com ID';
$lang['clients_partners_act_delete_record'] = 'Excluído registro com ID';

//Listing Specifics
$lang['clients_partners_records_empty']    = 'Nenhum registro encontrado.';
$lang['clients_partners_errors_message']    = 'Por favor corrija os erros a seguir:';

// Column Headings
$lang['clients_partners_column_created']  = 'Criado';
$lang['clients_partners_column_deleted']  = 'Excluído';
$lang['clients_partners_column_modified'] = 'Atualizado';

// Module Details
$lang['clients_partners_module_name'] = 'Clients-Partners';
$lang['clients_partners_module_description'] = 'Your module description';
$lang['clients_partners_area_title'] = 'Clients-Partners';

// Fields
$lang['clients_partners_field_name'] = 'Name';
$lang['clients_partners_field_image'] = 'Image';
$lang['clients_partners_field_partner'] = 'Partner';
