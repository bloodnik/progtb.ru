<?php defined('BASEPATH') || exit('No direct script access allowed');


$lang['clients_partners_manage']      = 'Управление клиентами';
$lang['clients_partners_edit']        = 'Редактировать';
$lang['clients_partners_true']        = 'Да';
$lang['clients_partners_false']       = 'Нет';
$lang['clients_partners_create']      = 'Создать';
$lang['clients_partners_list']        = 'Список клиентов(партнеров)';
$lang['clients_partners_new']       = 'Новый элемент';
$lang['clients_partners_edit_text']     = 'Отредактируте элемент если вам это необходимо';
$lang['clients_partners_no_records']    = 'Нет ни одной записи.';
$lang['clients_partners_create_new']    = 'Добавить новый элемент.';
$lang['clients_partners_create_success']  = 'Элемент успешно добавлен.';
$lang['clients_partners_create_failure']  = 'Есть некоторые проблемы при создании: ';
$lang['clients_partners_create_new_button'] = 'Создать новые элементы';
$lang['clients_partners_invalid_id']    = 'Неверный ID элемента.';
$lang['clients_partners_edit_success']    = 'Элемент успешно сохранен.';
$lang['clients_partners_edit_failure']    = 'Есть проблемы при сохранении: ';
$lang['clients_partners_delete_success']  = 'Запись(и) успешно удалена.';
$lang['clients_partners_delete_failure']  = 'Не возможно удалить элемент: ';
$lang['clients_partners_delete_error']    = 'Не выбран ни один элемент для удаления.';
$lang['clients_partners_actions']     = 'Действия';
$lang['clients_partners_cancel']      = 'Отмена';
$lang['clients_partners_delete_record']   = 'Удалить этот элемент';
$lang['clients_partners_delete_confirm']  = 'Вы действительно хотите удалить этот элемент?';
$lang['clients_partners_edit_heading']    = 'Редактирование элемента';

// Create/Edit Buttons
$lang['clients_partners_action_edit']   = 'Сохранить элемент';
$lang['clients_partners_action_create']   = 'Создать элемент';

// Activities
$lang['clients_partners_act_create_record'] = 'Создать запись с ID';
$lang['clients_partners_act_edit_record'] = 'Обновить запись с ID';
$lang['clients_partners_act_delete_record'] = 'Удалить запись с ID';

//Listing Specifics
$lang['clients_partners_records_empty']    = 'Не найдены удовлетворяющие вас записи.';
$lang['clients_partners_errors_message']    = 'Пожалуйста, исправте следующие проблемы:';

// Column Headings
$lang['clients_partners_column_created']  = 'Создано';
$lang['clients_partners_column_deleted']  = 'Удалено';
$lang['clients_partners_column_modified'] = 'Обновлено';
$lang['clients_partners_column_deleted_by'] = 'Удалено ';
$lang['clients_partners_column_created_by'] = 'Создано';
$lang['clients_partners_column_modified_by'] = 'Обновлено';

// Module Details
$lang['clients_partners_module_name'] = 'Элементы';
$lang['clients_partners_module_description'] = 'Клиенты(Партнеры)';
$lang['clients_partners_area_title'] = 'Клиенты(Партнеры)';



// Fields
$lang['clients_partners_field_name'] = 'Наименование';
$lang['clients_partners_field_link'] = 'Ссылка';
$lang['clients_partners_field_image'] = 'Изображение';
$lang['clients_partners_field_partner'] = 'Партнер?';