<?php defined('BASEPATH') || exit('No direct script access allowed');


$lang['clients_partners_manage']      = 'Manage Clients-Partners';
$lang['clients_partners_edit']        = 'Edit';
$lang['clients_partners_true']        = 'True';
$lang['clients_partners_false']       = 'False';
$lang['clients_partners_create']      = 'Create';
$lang['clients_partners_list']        = 'List';
$lang['clients_partners_new']       = 'New';
$lang['clients_partners_edit_text']     = 'Edit this to suit your needs';
$lang['clients_partners_no_records']    = 'There are no clients_partners in the system.';
$lang['clients_partners_create_new']    = 'Create a new Clients-Partners.';
$lang['clients_partners_create_success']  = 'Clients-Partners successfully created.';
$lang['clients_partners_create_failure']  = 'There was a problem creating the clients_partners: ';
$lang['clients_partners_create_new_button'] = 'Create New Clients-Partners';
$lang['clients_partners_invalid_id']    = 'Invalid Clients-Partners ID.';
$lang['clients_partners_edit_success']    = 'Clients-Partners successfully saved.';
$lang['clients_partners_edit_failure']    = 'There was a problem saving the clients_partners: ';
$lang['clients_partners_delete_success']  = 'record(s) successfully deleted.';
$lang['clients_partners_delete_failure']  = 'We could not delete the record: ';
$lang['clients_partners_delete_error']    = 'You have not selected any records to delete.';
$lang['clients_partners_actions']     = 'Actions';
$lang['clients_partners_cancel']      = 'Cancel';
$lang['clients_partners_delete_record']   = 'Delete this Clients-Partners';
$lang['clients_partners_delete_confirm']  = 'Are you sure you want to delete this clients_partners?';
$lang['clients_partners_edit_heading']    = 'Edit Clients-Partners';

// Create/Edit Buttons
$lang['clients_partners_action_edit']   = 'Save Clients-Partners';
$lang['clients_partners_action_create']   = 'Create Clients-Partners';

// Activities
$lang['clients_partners_act_create_record'] = 'Created record with ID';
$lang['clients_partners_act_edit_record'] = 'Updated record with ID';
$lang['clients_partners_act_delete_record'] = 'Deleted record with ID';

//Listing Specifics
$lang['clients_partners_records_empty']    = 'No records found that match your selection.';
$lang['clients_partners_errors_message']    = 'Please fix the following errors:';

// Column Headings
$lang['clients_partners_column_created']  = 'Created';
$lang['clients_partners_column_deleted']  = 'Deleted';
$lang['clients_partners_column_modified'] = 'Modified';
$lang['clients_partners_column_deleted_by'] = 'Deleted By';
$lang['clients_partners_column_created_by'] = 'Created By';
$lang['clients_partners_column_modified_by'] = 'Modified By';

// Module Details
$lang['clients_partners_module_name'] = 'Clients-Partners';
$lang['clients_partners_module_description'] = 'Your module description';
$lang['clients_partners_area_title'] = 'Clients-Partners';

// Fields
$lang['clients_partners_field_name'] = 'Name';
$lang['clients_partners_field_image'] = 'Image';
$lang['clients_partners_field_partner'] = 'Partner';