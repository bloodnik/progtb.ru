<?php defined('BASEPATH') || exit('No direct script access allowed');

$lang['clients_partners_manage']      = 'Gestionar Clients-Partners';
$lang['clients_partners_edit']        = 'Editar';
$lang['clients_partners_true']        = 'Verdadero';
$lang['clients_partners_false']       = 'Falso';
$lang['clients_partners_create']      = 'Crear';
$lang['clients_partners_list']        = 'Listar';
$lang['clients_partners_new']       = 'Nuevo';
$lang['clients_partners_edit_text']     = 'Editar esto para satisfacer sus necesidades';
$lang['clients_partners_no_records']    = 'Hay ninguna clients_partners en la sistema.';
$lang['clients_partners_create_new']    = 'Crear nuevo(a) Clients-Partners.';
$lang['clients_partners_create_success']  = 'Clients-Partners creado(a) con éxito.';
$lang['clients_partners_create_failure']  = 'Hubo un problema al crear el(la) clients_partners: ';
$lang['clients_partners_create_new_button'] = 'Crear nuevo(a) Clients-Partners';
$lang['clients_partners_invalid_id']    = 'ID de Clients-Partners inválido(a).';
$lang['clients_partners_edit_success']    = 'Clients-Partners guardado correctamente.';
$lang['clients_partners_edit_failure']    = 'Hubo un problema guardando el(la) clients_partners: ';
$lang['clients_partners_delete_success']  = 'Registro(s) eliminado con éxito.';
$lang['clients_partners_delete_failure']  = 'No hemos podido eliminar el registro: ';
$lang['clients_partners_delete_error']    = 'No ha seleccionado ning&#250;n registro que desea eliminar.';
$lang['clients_partners_actions']     = 'Açciones';
$lang['clients_partners_cancel']      = 'Cancelar';
$lang['clients_partners_delete_record']   = 'Eliminar este(a) Clients-Partners';
$lang['clients_partners_delete_confirm']  = '¿Esta seguro de que desea eliminar este(a) clients_partners?';
$lang['clients_partners_edit_heading']    = 'Editar Clients-Partners';

// Create/Edit Buttons
$lang['clients_partners_action_edit']   = 'Guardar Clients-Partners';
$lang['clients_partners_action_create']   = 'Crear Clients-Partners';

// Activities
$lang['clients_partners_act_create_record'] = 'Creado registro con ID';
$lang['clients_partners_act_edit_record'] = 'Actualizado registro con ID';
$lang['clients_partners_act_delete_record'] = 'Eliminado registro con ID';

//Listing Specifics
$lang['clients_partners_records_empty']    = 'No hay registros encontrados para su selección.';
$lang['clients_partners_errors_message']    = 'Por favor corrija los siguientes errores:';

// Column Headings
$lang['clients_partners_column_created']  = 'Creado';
$lang['clients_partners_column_deleted']  = 'Elíminado';
$lang['clients_partners_column_modified'] = 'Modificado';

// Module Details
$lang['clients_partners_module_name'] = 'Clients-Partners';
$lang['clients_partners_module_description'] = 'Your module description';
$lang['clients_partners_area_title'] = 'Clients-Partners';

// Fields
$lang['clients_partners_field_name'] = 'Name';
$lang['clients_partners_field_image'] = 'Image';
$lang['clients_partners_field_partner'] = 'Partner';
