<?php defined('BASEPATH') || exit('No direct script access allowed');

$lang['clients_partners_manage']      = 'Gestisci Clients-Partners';
$lang['clients_partners_edit']        = 'Modifica';
$lang['clients_partners_true']        = 'Vero';
$lang['clients_partners_false']       = 'Falso';
$lang['clients_partners_create']      = 'Crea';
$lang['clients_partners_list']        = 'Elenca';
$lang['clients_partners_new']       = 'Nuovo';
$lang['clients_partners_edit_text']     = 'Modifica questo secondo le tue necessità';
$lang['clients_partners_no_records']    = 'Non ci sono clients_partners nel sistema.';
$lang['clients_partners_create_new']    = 'Crea un nuovo Clients-Partners.';
$lang['clients_partners_create_success']  = 'Clients-Partners creato con successo.';
$lang['clients_partners_create_failure']  = 'C\'è stato un problema nella creazione di clients_partners: ';
$lang['clients_partners_create_new_button'] = 'Crea nuovo Clients-Partners';
$lang['clients_partners_invalid_id']    = 'ID Clients-Partners non valido.';
$lang['clients_partners_edit_success']    = 'Clients-Partners creato con successo.';
$lang['clients_partners_edit_failure']    = 'C\'è stato un errore nel salvataggio di clients_partners: ';
$lang['clients_partners_delete_success']  = 'record(s) creati con successo.';
$lang['clients_partners_delete_failure']  = 'Non possiamo eliminare il record: ';
$lang['clients_partners_delete_error']    = 'Non hai selezionato alcun record da eliminare.';
$lang['clients_partners_actions']     = 'Azioni';
$lang['clients_partners_cancel']      = 'Cancella';
$lang['clients_partners_delete_record']   = 'Elimina questo Clients-Partners';
$lang['clients_partners_delete_confirm']  = 'Sei sicuro di voler eliminare questo clients_partners?';
$lang['clients_partners_edit_heading']    = 'Modifica Clients-Partners';

// Create/Edit Buttons
$lang['clients_partners_action_edit']   = 'Salva Clients-Partners';
$lang['clients_partners_action_create']   = 'Crea Clients-Partners';

// Activities
$lang['clients_partners_act_create_record'] = 'Creato il record con ID';
$lang['clients_partners_act_edit_record'] = 'Aggiornato il record con ID';
$lang['clients_partners_act_delete_record'] = 'Eliminato il record con ID';

// Listing Specifics
$lang['clients_partners_records_empty']    = 'Nessun record trovato che corrisponda alla tua selezione.';
$lang['clients_partners_errors_message']    = 'Per favore risolvi i seguenti errori:';

// Column Headings
$lang['clients_partners_column_created']  = 'Creato';
$lang['clients_partners_column_deleted']  = 'Eliminato';
$lang['clients_partners_column_modified'] = 'Modificato';

// Module Details
$lang['clients_partners_module_name'] = 'Clients-Partners';
$lang['clients_partners_module_description'] = 'Your module description';
$lang['clients_partners_area_title'] = 'Clients-Partners';

// Fields
$lang['clients_partners_field_name'] = 'Name';
$lang['clients_partners_field_image'] = 'Image';
$lang['clients_partners_field_partner'] = 'Partner';
