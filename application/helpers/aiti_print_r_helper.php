<?php defined('BASEPATH') || exit('No direct script access allowed');


//Функция вывода массивов
function pr($item, $show_for = false) {

    if (!$item) echo ' <br />пусто <br />';
    elseif (is_array($item) && empty($item)) echo '<br />массив пуст <br />';
    else echo ' <pre>' . print_r($item, true) . ' </pre>';

}


/* End /helpers/aiti_file_helper.php */