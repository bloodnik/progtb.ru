<?php defined('BASEPATH') || exit('No direct script access allowed');

if ( ! function_exists('uploads_file_to_server')) {

    function uploads_file_to_server($data, $current_image_value='', $dir_path = "uploads", $config, $context)
    {
        if ($data['image']['error']==0) { //Елси выбран файл

            $context->load->library('upload', $config);

            if (!$context->upload->do_upload("image")) {
                $error = array('error' => $context->upload->display_errors());
                return $error;
            } else {
                $img_data = $context->upload->data();

                if (isset($data[$current_image_value]) && strlen($data[$current_image_value])>0){ //Если файл уже существует удаляем его для загрузки нового
                    unlink($str=substr($data[$current_image_value], 1));
                }
                return $dir_path.$img_data["file_name"];

            }
        }else{ //Если не выбран файл оставляем старое значение
            if (isset($data[$current_image_value]) && strlen($data[$current_image_value])>0) { //Если файл уже существует удаляем его для загрузки нового
                return $data[$current_image_value];
            }
        }

        return "";

    }
}

if ( ! function_exists('resize_img')) {


}
/* End /helpers/aiti_file_helper.php */