/** Slider */
$(window).load(function(){
	$('.flexslider').flexslider({animation: "slide"});
	
	$('.slider1').fractionSlider({
		'fullWidth': 			true,
		'pager': 				false,
	    'increase': 			false,
		'pauseOnHover': 		false,
		'slideEndAnimation': 	true,
		'timeout' : 1000
		
	});	
	
	
	$('.bp a').colorbox({transition:'elastic'});
});

function hideTab(num, this_header) {
	
	 var flag = $(this_header).attr("flag");
	
	if (flag == "true") {
		$(this_header).attr({flag: "false"});		
		$("#tab"+num + "_content").slideUp();
				
	}	
	else {
		$(this_header).attr({flag: "true"});	
		$("#tab"+num + "_content").slideDown();
				
	}	

}

$(document).ready(function(){	
	
	 $(".open_menu").click(function () {
	      $(".navigation ul").slideToggle("slow");
	    });

	// //Вычисление размера экрана и изменение высоты страницы
	// var bodyHeight;
	// bodyHeight = $("body").height();
	// $(".contaianer").css("height", bodyHeight);	
// 	
 	// $(window).resize(function() {	
 		// bodyHeight = $("body").height();
 		// $(".contaianer").css("height", bodyHeight);
	// });
// 	
// 	
	// $(".close").click(function(){
		// $(".overlay-wrap").fadeOut(500); 
		// $("body,html").animate({scrollTop:$('#blog').position().top - $('.navigation').height()});
	// });
// 	
	// //Подключаем плагин Onepage_Scroll
	// $("#fullpage").onepage_scroll({
	   // sectionContainer: "section",     // sectionContainer accepts any kind of selector in case you don't want to use section
	   // easing: "linear",                  // Easing options accepts the CSS3 easing animation such "ease", "linear", "ease-in", 
	                                    // // "ease-out", "ease-in-out", or even cubic bezier value such as "cubic-bezier(0.175, 0.885, 0.420, 1.310)"
	   // animationTime: 700,             // AnimationTime let you define how long each section takes to animate
	   // pagination: false,                // You can either show or hide the pagination. Toggle true for show, false for hide.
	   // updateURL: false,                // Toggle this true if you want the URL to be updated automatically when the user scroll to each page.
	   // beforeMove: function(index) {},  // This option accepts a callback function. The function will be called before the page moves.
	   // afterMove: function(index) {
			// if ($("body").hasClass("viewing-page-1")){
				// //alert("Первая страница");
				// $(".go-top").css("opacity", 0);
				// $(".right_menu").css("width", 0);
				// $("li>a").removeClass("active");
				// $("nav>ul>li").removeClass("active");
// 								
			// }		   	
			// else {
				// $(".go-top").css("opacity", 1);
				// $(".right_menu").css("width", 30);
				// $("li>a").removeClass("active");
				// $("nav>ul>li").removeClass("active");
				// if ($("body").hasClass("viewing-page-2")){
					// $("a[title='About Us']").addClass("active");
					// $("li[class='about']").addClass("active");
				// }
				// if ($("body").hasClass("viewing-page-3")){
					// $("a[title='Services']").addClass("active");
					// $("li[class='portfolio']").addClass("active");
				// }
				// if ($("body").hasClass("viewing-page-4")){
					// $("a[title='Testimonial']").addClass("active");
					// $("li[class='testimonial']").addClass("active");
				// }				
				// if ($("body").hasClass("viewing-page-5")){
					// $("a[title='Our Team']").addClass("active");
					// $("li[class='our_team']").addClass("active");
				// }					
				// if ($("body").hasClass("viewing-page-6")){
					// $("a[title='Contact']").addClass("active");
					// $("li[class='services']").addClass("active");
				// }									
			// }
	   // },   // This option accepts a callback function. The function will be called after the page moves.
	   // loop: false,                     // You can have the page loop back to the top/bottom when the user navigates at up/down on the first/last page.
	   // keyboard: true,                  // You can activate the keyboard controls
	   // responsiveFallback: false        // You can fallback to normal page scroll by defining the width of the browser in which
	                                    // // you want the responsive fallback to be triggered. For example, set this to 600 and whenever 
	                                    // // the browser's width is less than 600, the fallback will kick in.
	// });
// 	
	// //Проверка первой страницы(скрыаем кнопку вверх)
	// if ($("body").hasClass("viewing-page-1")){
		// //alert("Первая страница");
		// $(".go-top").css("opacity", 0);
		// $(".right_menu").css("width", 0);
		// $(".navigation_rigth ul li a small").css("width", 0);
	// }		
// 	
	// $(".navigation_rigth ul li").hover(
		// function() {$(this).find("small").css("width", "100px");},
		// function() {$(this).find("small").css("width", "0");}
	// )
	    	
});

$(window).load(function() {

		var lineChartData = {
			labels : ["Пн","Вт","Ср","Чт","Пт","Сб","Вс"],
			datasets : [
				{
					fillColor : "rgba(220,220,220,0.5)",
					strokeColor : "rgba(220,220,220,1)",
					pointColor : "rgba(220,220,220,1)",
					pointStrokeColor : "#fff",
					data : [2698,3863,3544,2164,535,10,23]
				},
				{
					fillColor : "rgba(151,187,205,0.5)",
					strokeColor : "rgba(151,187,205,1)",
					pointColor : "rgba(151,187,205,1)",
					pointStrokeColor : "#fff",
					data : [321,456,246,466,2345,256,144]
				}
			]
		};

		var doughnutChartData = [
				{
					value: 42,
					color:"#F7464A"
				},
				{
					value : 8,
					color : "#46BFBD"
				},
				{
					value : 33,
					color : "#FDB45C"
				},
				{
					value : 17,
					color : "#949FB1"
				}
			
		];
				
		var globalGraphSettings = {animation : Modernizr.canvas};
		
		function showLineChart(){
			var ctx = document.getElementById("lineChartCanvas").getContext("2d");
			new Chart(ctx).Line(lineChartData,globalGraphSettings);
		};
		
		function showDoughnutChart(){
			var ctx = document.getElementById("doughnutChartCanvas").getContext("2d");
			new Chart(ctx).Doughnut(doughnutChartData,globalGraphSettings);
		};
		function showDoughnutChart2(){
			var ctx = document.getElementById("doughnutChartCanvas2").getContext("2d");
			new Chart(ctx).Doughnut(doughnutChartData,globalGraphSettings);
		};
		
		
		var graphInitDelay = 300;
		
		$("#lineChart").on("inview",function(){
			var $this = $(this);
			$this.removeClass("hidden").off("inview");
			setTimeout(showLineChart,graphInitDelay);
		});	
	
	
		$("#doughnutChart").on("inview",function(){
			var $this = $(this);
			$this.removeClass("hidden").off("inview");
			setTimeout(showDoughnutChart,graphInitDelay);			
		});
		$("#doughnutChart2").on("inview",function(){
			var $this = $(this);
			$this.removeClass("hidden").off("inview");
			setTimeout(showDoughnutChart2,graphInitDelay);			
		});
		
	});
	
	/**
	 * author Christopher Blum
	 *    - based on the idea of Remy Sharp, http://remysharp.com/2009/01/26/element-in-view-event-plugin/
	 *    - forked from http://github.com/zuk/jquery.inview/
	 */
	(function ($) {
	  var inviewObjects = {}, viewportSize, viewportOffset,
	      d = document, w = window, documentElement = d.documentElement, expando = $.expando;
	
	  $.event.special.inview = {
	    add: function(data) {
	      inviewObjects[data.guid + "-" + this[expando]] = { data: data, $element: $(this) };
	    },
	
	    remove: function(data) {
	      try { delete inviewObjects[data.guid + "-" + this[expando]]; } catch(e) {}
	    }
	  };
	
	  function getViewportSize() {
	    var mode, domObject, size = { height: w.innerHeight, width: w.innerWidth };
	
	    // if this is correct then return it. iPad has compat Mode, so will
	    // go into check clientHeight/clientWidth (which has the wrong value).
	    if (!size.height) {
	      mode = d.compatMode;
	      if (mode || !$.support.boxModel) { // IE, Gecko
	        domObject = mode === 'CSS1Compat' ?
	          documentElement : // Standards
	          d.body; // Quirks
	        size = {
	          height: domObject.clientHeight,
	          width:  domObject.clientWidth
	        };
	      }
	    }
	
	    return size;
	  }
	
	  function getViewportOffset() {
	    return {
	      top:  w.pageYOffset || documentElement.scrollTop   || d.body.scrollTop,
	      left: w.pageXOffset || documentElement.scrollLeft  || d.body.scrollLeft
	    };
	  }
	
	  function checkInView() {
	    var $elements = $(), elementsLength, i = 0;
	
	    $.each(inviewObjects, function(i, inviewObject) {
	      var selector  = inviewObject.data.selector,
	          $element  = inviewObject.$element;
	      $elements = $elements.add(selector ? $element.find(selector) : $element);
	    });
	
	    elementsLength = $elements.length;
	    if (elementsLength) {
	      viewportSize   = viewportSize   || getViewportSize();
	      viewportOffset = viewportOffset || getViewportOffset();
	
	      for (; i<elementsLength; i++) {
	        // Ignore elements that are not in the DOM tree
	        if (!$.contains(documentElement, $elements[i])) {
	          continue;
	        }
	
	        var $element      = $($elements[i]),
	            elementSize   = { height: $element.height(), width: $element.width() },
	            elementOffset = $element.offset(),
	            inView        = $element.data('inview'),
	            visiblePartX,
	            visiblePartY,
	            visiblePartsMerged;
	        
	        // Don't ask me why because I haven't figured out yet:
	        // viewportOffset and viewportSize are sometimes suddenly null in Firefox 5.
	        // Even though it sounds weird:
	        // It seems that the execution of this function is interferred by the onresize/onscroll event
	        // where viewportOffset and viewportSize are unset
	        if (!viewportOffset || !viewportSize) {
	          return;
	        }
	        
	        if (elementOffset.top + elementSize.height > viewportOffset.top &&
	            elementOffset.top < viewportOffset.top + viewportSize.height &&
	            elementOffset.left + elementSize.width > viewportOffset.left &&
	            elementOffset.left < viewportOffset.left + viewportSize.width) {
	          visiblePartX = (viewportOffset.left > elementOffset.left ?
	            'right' : (viewportOffset.left + viewportSize.width) < (elementOffset.left + elementSize.width) ?
	            'left' : 'both');
	          visiblePartY = (viewportOffset.top > elementOffset.top ?
	            'bottom' : (viewportOffset.top + viewportSize.height) < (elementOffset.top + elementSize.height) ?
	            'top' : 'both');
	          visiblePartsMerged = visiblePartX + "-" + visiblePartY;
	          if (!inView || inView !== visiblePartsMerged) {
	            $element.data('inview', visiblePartsMerged).trigger('inview', [true, visiblePartX, visiblePartY]);
	          }
	        } else if (inView) {
	          $element.data('inview', false).trigger('inview', [false]);
	        }
	      }
	    }
	  }
	
	  $(w).bind("scroll resize", function() {
	    viewportSize = viewportOffset = null;
	  });
	  
	  // IE < 9 scrolls to focused elements without firing the "scroll" event
	  if (!documentElement.addEventListener && documentElement.attachEvent) {
	    documentElement.attachEvent("onfocusin", function() {
	      viewportOffset = null;
	    });
	  }
	
	  // Use setInterval in order to also make sure this captures elements within
	  // "overflow:scroll" elements or elements that appeared in the dom tree due to
	  // dom manipulation and reflow
	  // old: $(window).scroll(checkInView);
	  //
	  // By the way, iOS (iPad, iPhone, ...) seems to not execute, or at least delays
	  // intervals while the user scrolls. Therefore the inview event might fire a bit late there
	  setInterval(checkInView, 250);
	})(jQuery);	