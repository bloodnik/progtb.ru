<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.2.0
Version: 3.3.1
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest (the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<!-- Head BEGIN -->
<head>
  <meta charset="utf-8">
  <title><?=$title?></title>

  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

  <meta content="Блог компании ООО 'Прогтехбизнес'" name="description">
  <meta content="" name="keywords">
  <meta content="" name="author">

  <meta property="og:site_name" content="Официальный блог ООО 'Прогтехбизнес'">
  <meta property="og:title" content="<?=$title?>">
  <meta property="og:description" content="">
  <meta property="og:type" content="website">
  <meta property="og:image" content=""><!-- link to image for socio -->
  <meta property="og:url" content="">

  <link rel="shortcut icon" href="favicon.ico">

  <!-- Fonts START -->
  <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700|PT+Sans+Narrow|Source+Sans+Pro:200,300,400,600,700,900&amp;subset=all" rel="stylesheet" type="text/css">
  <!-- Fonts END -->

  <!-- Global styles START -->          
  <link href="/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Global styles END --> 
   
  <!-- Page level plugin styles START -->
  <link href="/assets/global/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet">
  <!-- Page level plugin styles END -->

  <!-- Theme styles START -->
  <link href="/assets/global/css/components.css" rel="stylesheet">
  <link href="/assets/frontend/layout/css/style.css" rel="stylesheet">
  <link href="/assets/frontend/layout/css/style-responsive.css" rel="stylesheet">
  <link href="/assets/frontend/layout/css/themes/blue.css" rel="stylesheet" id="style-color">
  <link href="/assets/frontend/layout/css/custom.css" rel="stylesheet">

  <!-- Theme styles END -->
</head>
<!-- Head END -->