<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="ru" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="ru" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="ru">
<!--<![endif]-->
<head>
  <meta charset="utf-8">
  <title>Прогрессивные Технологии Бизнеса - ПрогТехБизнес - ПТБ - Самый официальный сайт</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta content="Официальный сайт компании Прогрессивные Технологии Бизнеса. Лидер в области разработки, автоматизации и внедрения бизнес-решений, широко используемых в целях управления организацией." name="description">
  <meta content="ПТБ, ПрогТехБизнес, Прогрессивные Технологии Бизнеса, автоматизация бизнеса, бизнес решения, автоматизация управления организацией" name="keywords">
  <meta content="Прогрессивные Технологии Бизнеса" name="author">
  <meta http-equiv="cleartype" content="on">
  <link rel="shortcut icon" href="favicon.ico">
  <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700|Pathway+Gothic+One|PT+Sans+Narrow:400+700|Source+Sans+Pro:200,300,400,600,700,900&amp;subset=all" rel="stylesheet" type="text/css"> 
  <link href="assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="assets/global/plugins/slider-revolution-slider/rs-plugin/css/settings.css" rel="stylesheet">
  <link href="assets/global/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet">
  <link href="assets/global/css/components.css" rel="stylesheet">
  <link href="assets/frontend/onepage/css/style.css" rel="stylesheet">
  <link href="assets/frontend/onepage/css/style-responsive.css" rel="stylesheet">
  <link href="assets/frontend/onepage/css/themes/blue.css" rel="stylesheet" id="style-color">

  <link rel="stylesheet" href="assets/global/plugins/countdown/plugin/jquery.countdown.css">
  <!-- link rel="stylesheet" href="assets/global/css/animate.min.css" -->

  <link href="assets/frontend/onepage/css/custom.css" rel="stylesheet">
    <link href="assets/admin/layout3/css/layout.css" rel="stylesheet" type="text/css">
    <link href="assets/admin/layout3/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color">
    <link href="assets/admin/layout3/css/custom.css" rel="stylesheet" type="text/css">
    <link href="assets/admin/pages/css/login-soft.css" rel="stylesheet" type="text/css"/>
    <link href="assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css">
</head>